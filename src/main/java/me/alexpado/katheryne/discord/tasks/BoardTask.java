package me.alexpado.katheryne.discord.tasks;

import me.alexpado.katheryne.commissions.entities.Board;
import me.alexpado.katheryne.commissions.entities.Commission;
import me.alexpado.katheryne.commissions.entities.Notification;
import me.alexpado.katheryne.commissions.events.CommissionUpdateEvent;
import me.alexpado.katheryne.commissions.repositories.BoardRepository;
import me.alexpado.katheryne.commissions.repositories.CommissionRepository;
import me.alexpado.katheryne.commissions.repositories.NotificationRepository;
import me.alexpado.katheryne.discord.listeners.JdaStore;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.stream.Collectors;

@Service
public class BoardTask implements Runnable, ApplicationListener<CommissionUpdateEvent> {

    private final BoardRepository        boardRepository;
    private final CommissionRepository   commissionRepository;
    private final NotificationRepository notificationRepository;
    private final JdaStore               store;

    public BoardTask(BoardRepository boardRepository, CommissionRepository commissionRepository, NotificationRepository notificationRepository, JdaStore store) {
        this.boardRepository        = boardRepository;
        this.commissionRepository   = commissionRepository;
        this.notificationRepository = notificationRepository;
        this.store                  = store;
    }

    @Override
    @Scheduled(cron = "0/1 * * * * ?")
    public void run() {
        Logger logger = LoggerFactory.getLogger(BoardTask.class);

        JDA         jda    = this.store.getJda();
        List<Board> boards = this.boardRepository.findAllByRunnable();

        if (boards.isEmpty() || jda == null) {
            return;
        }

        Board board = boards.get(0);

        Guild guild = jda.getGuildById(board.getGuild().getId());

        if (guild == null) {
            logger.info("Board {} | Cancelling refresh: Server not found ???", board.getId());
            board.setRefreshable(false);
            board.setNotifiable(false);
            this.boardRepository.save(board);
            return;
        }

        Member      self    = guild.getSelfMember();
        TextChannel channel = guild.getTextChannelById(board.getChannelId());

        if (channel == null || !channel.canTalk() || !self.hasPermission(channel, Permission.MESSAGE_EMBED_LINKS)) {
            logger.info("Board {} | Cancelling refresh: Channel not found or not enough permissions.", board.getId());
            board.setRefreshable(false);
            board.setNotifiable(false);
            this.boardRepository.save(board);
            return;
        }

        if (board.isRefreshable()) {
            Message message = board.createMessage(channel);

            if (message == null) {
                logger.info("Board {} | Cancelling refresh: The message could not be found or sent.", board.getId());
                board.setRefreshable(false);
                this.boardRepository.save(board);
                return;
            }

            logger.info("Board {} | Displaying commission...", board.getId());
            EmbedBuilder embed = this.getCommissionBoard(board);
            message.editMessageEmbeds(embed.build()).complete();
            board.setRefreshable(false);
        }

        if (board.isNotifiable()) {
            List<Notification> notifications = this.notificationRepository.findAllByBoardId(board.getId());
            notifications = notifications.stream()
                    .filter(Notification::isEnabled)
                    .collect(Collectors.toList());

            for (Notification notification : notifications) {
                logger.info("Board {} | Sending {} notification: {}", board.getId(), notification.getRegion(), notification.getEffectiveContent());
                notification.send(channel);
            }

            this.notificationRepository.saveAll(notifications);
            board.setNotifiable(false);
        }

        this.boardRepository.save(board);
    }


    private EmbedBuilder getCommissionBoard(Board board) {

        List<Commission> commissions;

        if (board.getRegion() == ServerRegion.ALL) {
            commissions = this.commissionRepository.findAllByClosedIsFalse();
        } else {
            commissions = this.commissionRepository.findAllByClosedIsFalseAndProfileRegion(board.getRegion());
        }

        if (!commissions.isEmpty()) {
            LinkedBlockingDeque<Commission> dequeue = new LinkedBlockingDeque<>(commissions);

            EmbedBuilder builder = new EmbedBuilder();

            while (!dequeue.isEmpty() && builder.length() < Message.MAX_CONTENT_LENGTH) {
                Commission commission = dequeue.pollFirst();
                // This should always be true in the while loop, this is just to calm down compiler warnings
                if (commission != null) {
                    builder.addField(commission.toField());
                }
            }

            return builder;
        } else {
            return new EmbedBuilder().setDescription("*No commissions available for Adventurers at this time*");
        }
    }

    /**
     * Handle an application event.
     *
     * @param event
     *         the event to respond to
     */
    @Override
    public void onApplicationEvent(@NotNull CommissionUpdateEvent event) {

        ServerRegion region = event.getCommission().getProfile().getRegion();
        List<Board>  boards = this.boardRepository.findAllByRegionIn(Arrays.asList(region, ServerRegion.ALL));
        boards.forEach(board -> {
            board.setRefreshable(true);
            board.setNotifiable(!event.getCommission().isClosed());

            if (board.isNotifiable()) {
                List<Notification> notifications = this.notificationRepository.findAllByBoardId(board.getId());
                notifications.forEach(notification -> notification.setEnabled(notification.isRegionCompatible(event.getCommission().getProfile().getRegion())));
                this.notificationRepository.saveAll(notifications);
            }

        });
        this.boardRepository.saveAll(boards);
    }
}
