package me.alexpado.katheryne.discord.tasks;

import me.alexpado.katheryne.commissions.entities.Commission;
import me.alexpado.katheryne.commissions.events.CommissionUpdateEvent;
import me.alexpado.katheryne.commissions.repositories.CommissionRepository;
import me.alexpado.katheryne.discord.listeners.JdaStore;
import me.alexpado.katheryne.messages.commissions.CommissionAutoClosedEmbed;
import net.dv8tion.jda.api.MessageBuilder;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommissionAutoClosingTask implements Runnable {

    private final CommissionRepository      repository;
    private final ApplicationEventPublisher publisher;
    private final JdaStore                  store;

    public CommissionAutoClosingTask(CommissionRepository repository, ApplicationEventPublisher publisher, JdaStore store) {

        this.repository = repository;
        this.publisher  = publisher;
        this.store      = store;
    }

    @Override
    @Scheduled(cron = "0/1 * * * * ?")
    public void run() {

        if (this.store.getJda() == null) {
            return;
        }

        List<Commission> commissions = this.repository.findAllByClosedIsFalse();

        if (commissions.isEmpty()) {
            return;
        }

        Commission commission = commissions.get(0);


        if (commission.autoClose()) {

            MessageBuilder builder = new MessageBuilder();

            builder.setContent("Traveler, your commission has been auto-closed. Feel free to open a new one.");
            builder.setEmbeds(new CommissionAutoClosedEmbed(this.store.getJda(), commission).build());

            this.store.getJda().retrieveUserById(commission.getProfile().getTraveler().getId()).queue(
                    user -> user.openPrivateChannel().queue(
                            channel -> channel.sendMessage(builder.build()).queue()
                    )
            );

            this.repository.save(commission);
            this.publisher.publishEvent(new CommissionUpdateEvent(this, commission));
        }

    }
}
