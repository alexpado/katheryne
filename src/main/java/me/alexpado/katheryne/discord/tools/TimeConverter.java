package me.alexpado.katheryne.discord.tools;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeConverter {

    private final long hours;
    private       long minutes;
    private       long seconds;

    public TimeConverter(long duration) {

        this.hours   = TimeUnit.HOURS.convert(duration, TimeUnit.SECONDS);
        this.minutes = TimeUnit.MINUTES.convert(duration, TimeUnit.SECONDS);
        this.seconds = duration;
        this.minutes -= this.hours * 60;
        this.seconds -= (this.hours * 3600) + (this.minutes * 60);
    }

    public static long fromString(String str) {

        String  regex   = "(?<weeks>\\d*w)?(?<days>\\d*d)?(?<hours>\\d*h)?(?<minutes>\\d+m)?";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        long    time    = 0L;
        if (matcher.find()) {
            String week   = matcher.group("weeks");
            String day    = matcher.group("days");
            String hour   = matcher.group("hours");
            String minute = matcher.group("minutes");

            if (week != null) {
                time += Long.parseLong(week.replace("w", "")) * 7 * 24 * 60 * 60;
            }

            if (day != null) {
                time += Long.parseLong(day.replace("d", "")) * 24 * 60 * 60;
            }

            if (hour != null) {
                time += Long.parseLong(hour.replace("h", "")) * 60 * 60;
            }

            if (minute != null) {
                time += Long.parseLong(minute.replace("m", "")) * 60;
            }
        }
        return time;
    }

    @Override
    public String toString() {

        if (this.hours != 0) {
            if (this.seconds != 0) {
                return String.format("%sh%02dm%02ds", this.hours, this.minutes, this.seconds);
            } else if (this.minutes != 0) {
                return String.format("%sh%02dm", this.hours, this.minutes);
            } else {
                return String.format("%sh", this.hours);
            }
        } else if (this.minutes != 0) {
            if (this.seconds != 0) {
                return String.format("%02dm%02ds", this.minutes, this.seconds);
            }
            return String.format("%sm", this.minutes);
        }
        return String.format("%02ds", this.seconds);
    }

}
