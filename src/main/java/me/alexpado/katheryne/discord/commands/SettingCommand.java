package me.alexpado.katheryne.discord.commands;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.DiscordCommand;
import fr.alexpado.jda.services.commands.annotations.Command;
import fr.alexpado.jda.services.commands.annotations.Param;
import fr.alexpado.jda.services.commands.interfaces.ICommand;
import fr.alexpado.jda.services.commands.interfaces.ICommandMeta;
import me.alexpado.katheryne.discord.commands.meta.SettingCommandMeta;
import me.alexpado.katheryne.discord.interfaces.IKatheryneContext;
import me.alexpado.katheryne.messages.base.NotEnoughPermissionEmbed;
import me.alexpado.katheryne.messages.helps.SettingHelpEmbed;
import me.alexpado.katheryne.messages.settings.InvalidPrefixEmbed;
import me.alexpado.katheryne.messages.settings.PrefixUpdatedEmbed;
import me.alexpado.katheryne.messages.settings.WrongUserTypeEmbed;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import org.springframework.stereotype.Component;

@Component
public class SettingCommand extends DiscordCommand {

    /**
     * Creates a new {@link DiscordCommand} instance and register it immediately.
     *
     * @param discordBot
     *         The {@link IDiscordBot} associated with this command.
     */
    protected SettingCommand(IDiscordBot discordBot) {
        super(discordBot);
        discordBot.getCommandHandler().register(this);
    }

    /**
     * Retrieve the command meta for this {@link ICommand}.
     *
     * @return An {@link ICommandMeta} instance.
     */
    @Override
    public ICommandMeta getMeta() {
        return new SettingCommandMeta();
    }

    @Command("help")
    public EmbedBuilder getHelp(IKatheryneContext context) {
        return new SettingHelpEmbed(context, this.getBot());
    }

    @Command("server prefix [prefix]")
    public EmbedBuilder defineSetting(IKatheryneContext context, @Param("prefix") String prefix) {

        // Check for user's permission first
        Member member = context.getEvent().getMember();

        if (member == null) {
            return new WrongUserTypeEmbed(context);
        }

        if (!member.isOwner() && !member.hasPermission(Permission.ADMINISTRATOR) && !member.hasPermission(Permission.MANAGE_SERVER)) {
            return new NotEnoughPermissionEmbed(context);
        }

        if (prefix.trim().isEmpty()) {
            return new InvalidPrefixEmbed(context);
        }

        context.getAdventurerGuild().setPrefix(prefix.trim());

        return new PrefixUpdatedEmbed(context);
    }

}
