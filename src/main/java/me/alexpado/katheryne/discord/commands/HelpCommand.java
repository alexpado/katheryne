package me.alexpado.katheryne.discord.commands;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.DiscordCommand;
import fr.alexpado.jda.services.commands.interfaces.ICommand;
import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import fr.alexpado.jda.services.commands.interfaces.ICommandHandler;
import fr.alexpado.jda.services.commands.interfaces.ICommandMeta;
import me.alexpado.katheryne.discord.commands.meta.HelpCommandMeta;
import me.alexpado.katheryne.messages.helps.GeneralHelpEmbed;
import net.dv8tion.jda.api.entities.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public class HelpCommand extends DiscordCommand {

    /**
     * Creates a new {@link DiscordCommand} instance and register it immediately.
     *
     * @param discordBot
     *         The {@link IDiscordBot} associated with this command.
     */
    protected HelpCommand(IDiscordBot discordBot) {

        super(discordBot);
        discordBot.getCommandHandler().register(this);
    }

    /**
     * Retrieve the command meta for this {@link ICommand}.
     *
     * @return An {@link ICommandMeta} instance.
     */
    @Override
    public ICommandMeta getMeta() {

        return new HelpCommandMeta();
    }

    /**
     * Called by the {@link ICommandHandler} when an {@link User} execute this command.
     *
     * @param context
     *         The {@link ICommandContext} for the current command's execution flow.
     */
    @Override
    public Object execute(@NotNull ICommandContext context) {

        // Overriding the implementation as we don't even need to execute command parsing \o/
        return new GeneralHelpEmbed(context, this.getBot());
    }
}
