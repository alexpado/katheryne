package me.alexpado.katheryne.discord.commands;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.DiscordCommand;
import fr.alexpado.jda.services.commands.annotations.Command;
import fr.alexpado.jda.services.commands.annotations.Param;
import fr.alexpado.jda.services.commands.interfaces.ICommand;
import fr.alexpado.jda.services.commands.interfaces.ICommandMeta;
import me.alexpado.katheryne.discord.commands.meta.NoticeCommandMeta;
import me.alexpado.katheryne.discord.interfaces.IKatheryneContext;
import me.alexpado.katheryne.messages.base.NotEnoughPermissionEmbed;
import net.dv8tion.jda.api.EmbedBuilder;
import org.springframework.stereotype.Component;

@Component
public class NoticeCommand extends DiscordCommand {

    /**
     * Creates a new {@link DiscordCommand} instance and register it immediately.
     *
     * @param discordBot
     *         The {@link IDiscordBot} associated with this command.
     */
    protected NoticeCommand(IDiscordBot discordBot) {
        super(discordBot);
        discordBot.getCommandHandler().register(this);
    }

    /**
     * Retrieve the command meta for this {@link ICommand}.
     *
     * @return An {@link ICommandMeta} instance.
     */
    @Override
    public ICommandMeta getMeta() {
        return new NoticeCommandMeta();
    }

    @Command("set message...")
    public EmbedBuilder setNotice(IKatheryneContext context, @Param("message") String message) {

        if (!context.getTraveler().isAdmin()) {
            return new NotEnoughPermissionEmbed(context);
        }

        this.getBot().getCommandHandler().setNotice(message);
        return new EmbedBuilder().setDescription("Notice message defined.");
    }

    @Command("remove")
    public EmbedBuilder removeNotice(IKatheryneContext context) {

        this.getBot().getCommandHandler().setNotice(null);
        return new EmbedBuilder().setDescription("Notice message deleted.");
    }
}
