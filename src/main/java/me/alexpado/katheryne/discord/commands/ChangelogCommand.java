package me.alexpado.katheryne.discord.commands;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.DiscordCommand;
import fr.alexpado.jda.services.commands.interfaces.ICommand;
import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import fr.alexpado.jda.services.commands.interfaces.ICommandHandler;
import fr.alexpado.jda.services.commands.interfaces.ICommandMeta;
import me.alexpado.katheryne.discord.commands.meta.ChangelogCommandMeta;
import me.alexpado.katheryne.messages.changelogs.ChangelogEmbed;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public class ChangelogCommand extends DiscordCommand {

    /**
     * Creates a new {@link DiscordCommand} instance and register it immediately.
     *
     * @param discordBot
     *         The {@link IDiscordBot} associated with this command.
     */
    public ChangelogCommand(IDiscordBot discordBot) {
        super(discordBot);

        discordBot.getCommandHandler().register(this);
    }

    /**
     * Retrieve the command meta for this {@link ICommand}.
     *
     * @return An {@link ICommandMeta} instance.
     */
    @Override
    public ICommandMeta getMeta() {
        return new ChangelogCommandMeta();
    }

    /**
     * Called by the {@link ICommandHandler} when an {@link User} execute this command.
     *
     * @param context
     *         The {@link MessageReceivedEvent} for the current command's execution flow.
     */
    @Override
    public Object execute(@NotNull ICommandContext context) {
        return new ChangelogEmbed(context);
    }
}
