package me.alexpado.katheryne.discord.commands;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.DiscordCommand;
import fr.alexpado.jda.services.commands.annotations.Command;
import fr.alexpado.jda.services.commands.annotations.Param;
import fr.alexpado.jda.services.commands.interfaces.ICommandMeta;
import me.alexpado.katheryne.GenshinCoopApplication;
import me.alexpado.katheryne.discord.commands.meta.DevToolCommandMeta;
import me.alexpado.katheryne.discord.interfaces.IKatheryneContext;
import me.alexpado.katheryne.messages.base.NotEnoughPermissionEmbed;
import net.dv8tion.jda.api.EmbedBuilder;
import org.aspectj.bridge.ICommand;
import org.springframework.stereotype.Component;

@Component
public class DevToolCommand extends DiscordCommand {

    /**
     * Creates a new {@link DiscordCommand} instance and register it immediately.
     *
     * @param discordBot
     *         The {@link IDiscordBot} associated with this command.
     */
    protected DevToolCommand(IDiscordBot discordBot) {
        super(discordBot);
        discordBot.getCommandHandler().register(this);
    }

    /**
     * Retrieve the command meta for this {@link ICommand}.
     *
     * @return An {@link ICommandMeta} instance.
     */
    @Override
    public ICommandMeta getMeta() {
        return new DevToolCommandMeta();
    }

    @Command("feature uid lock reason...")
    public EmbedBuilder lockUidRelatedFeature(IKatheryneContext context, @Param("reason") String reason) {

        if (!context.getTraveler().isAdmin()) {
            return new NotEnoughPermissionEmbed(context);
        }
        GenshinCoopApplication.GLOBAL_UID_LOCK = true;
        this.getBot().getCommandHandler().setNotice(reason);

        return new EmbedBuilder().setDescription("UID-related feature are now disabled.");
    }

    @Command("feature uid unlock")
    public EmbedBuilder unlockUidRelatedFeature(IKatheryneContext context) {

        if (!context.getTraveler().isAdmin()) {
            return new NotEnoughPermissionEmbed(context);
        }
        GenshinCoopApplication.GLOBAL_UID_LOCK = false;
        this.getBot().getCommandHandler().setNotice(null);

        return new EmbedBuilder().setDescription("UID-related feature are now enabled.");
    }

    @Command("echo message...")
    public EmbedBuilder echoBackRawMessage(IKatheryneContext context, @Param("message") String message) {
        return new EmbedBuilder().appendDescription("```").appendDescription(message).appendDescription("```");
    }
}
