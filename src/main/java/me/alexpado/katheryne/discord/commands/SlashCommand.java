package me.alexpado.katheryne.discord.commands;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.DiscordCommand;
import fr.alexpado.jda.services.commands.interfaces.ICommand;
import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import fr.alexpado.jda.services.commands.interfaces.ICommandHandler;
import fr.alexpado.jda.services.commands.interfaces.ICommandMeta;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.awt.*;

/**
 * Temporary command
 */
@Component
public class SlashCommand extends DiscordCommand {

    /**
     * Creates a new {@link DiscordCommand} instance and register it immediately.
     *
     * @param discordBot
     *         The {@link IDiscordBot} associated with this command.
     */
    public SlashCommand(IDiscordBot discordBot) {
        super(discordBot);
        discordBot.getCommandHandler().register(this);
    }

    /**
     * Retrieve the command meta for this {@link ICommand}.
     *
     * @return An {@link ICommandMeta} instance.
     */
    @Override
    public ICommandMeta getMeta() {
        return new ICommandMeta() {
            @Override
            public String getLabel() {
                return "slash";
            }

            @Override
            public String getDescription() {
                return "Get information about slash commands.";
            }

            @Override
            public String getHelp() {
                return null;
            }
        };
    }

    /**
     * Called by the {@link ICommandHandler} when an {@link User} execute this command.
     *
     * @param context
     *         The {@link MessageReceivedEvent} for the current command's execution flow.
     *
     * @throws Exception
     *         Thrown if something happen during the command's execution.
     */
    @Override
    public Object execute(@NotNull ICommandContext context) throws Exception {

        return new EmbedBuilder() {{
            this.setColor(Color.CYAN);
            this.setTitle("More about Slash Commands");
            this.setDescription("First thing first, if you don't know what are slash commands, I recommend you to go read [this article from Discord](https://support.discord.com/hc/fr/articles/1500000368501-Slash-Commands-FAQ).");

            this.addField("For users", """
                            Regular commands (using the bot's prefix which is by default `coop@`) will soon be removed due to Discord internal change. There is nothing I can do about.
                            To be honest, slash commands are easier to use than the actual mess that I coded, both for you and me, so I don't really see a problem here.
                            """
                    , false);

            this.addField("But wait, where are they ?", """
                    The bot probably doesn't show any command when typing `/` right ? Well about that...
                    *If you're not the server admin, please tell them to read this !*

                    This is a change in permissions made by Discord where something need to be added to the bot invitation link. Without it, the bot is unable to create slash commands in your server...

                    I updated the invitation link, you just have to click it and accept new permissions.
                    
                    Or, if you are lazy to find the invitation link, you can [click here instead](https://discord.com/api/oauth2/authorize?client_id=797271454180966421&permissions=26624&scope=bot%20applications.commands).
                    """, false);

            this.addField("Need more help ? Have some question ?", "Head over to our support server by [clicking here](https://discord.com/invite/b9SggV5vqU).", false);
        }};

    }
}
