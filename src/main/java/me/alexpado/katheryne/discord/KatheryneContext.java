package me.alexpado.katheryne.discord;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.discord.interfaces.IKatheryneContext;
import me.alexpado.katheryne.genshin.entities.AdventurerGuild;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.genshin.repositories.AdventurerGuildRepository;
import me.alexpado.katheryne.genshin.repositories.TravelerRepository;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class KatheryneContext implements IKatheryneContext {

    private final MessageReceivedEvent      event;
    private final TravelerRepository        travelerRepository;
    private final AdventurerGuildRepository adventurerGuildRepository;
    private final Traveler                  traveler;
    private final AdventurerGuild           adventurerGuild;

    public KatheryneContext(MessageReceivedEvent event, TravelerRepository travelerRepository, AdventurerGuildRepository adventurerGuildRepository, Traveler traveler, AdventurerGuild adventurerGuild) {

        this.event                     = event;
        this.travelerRepository        = travelerRepository;
        this.adventurerGuildRepository = adventurerGuildRepository;
        this.traveler                  = traveler;
        this.adventurerGuild           = adventurerGuild;
    }

    /**
     * Retrieves a {@link TravelerRepository} allowing interaction with the database for the {@link Traveler} entity.
     *
     * @return A {@link TravelerRepository}.
     */
    @Override
    public TravelerRepository getTravelerRepository() {
        return this.travelerRepository;
    }

    /**
     * Retrieves the current {@link Traveler} responsible for this {@link IKatheryneContext}'s creation.
     *
     * @return A {@link Traveler} instance.
     */
    @Override
    public Traveler getTraveler() {
        return this.traveler;
    }

    /**
     * Retrieves a {@link AdventurerGuildRepository} allowing interaction with the database for the {@link
     * AdventurerGuild} entity.
     *
     * @return A {@link AdventurerGuild}.
     */
    @Override
    public AdventurerGuildRepository getAdventurerGuildRepository() {
        return this.adventurerGuildRepository;
    }

    /**
     * Retrieves the current {@link AdventurerGuild} responsible for this {@link IKatheryneContext}'s creation.
     *
     * @return An {@link AdventurerGuild} instance.
     */
    @Override
    public AdventurerGuild getAdventurerGuild() {
        return this.adventurerGuild;
    }

    @Override
    public void close() {
        this.getTravelerRepository().save(this.getTraveler());
        this.getAdventurerGuildRepository().save(this.getAdventurerGuild());
    }

    /**
     * Retrieves the {@link MessageReceivedEvent} that caused this {@link ICommandContext} to be created.
     *
     * @return A {@link MessageReceivedEvent}.
     */
    @Override
    public MessageReceivedEvent getEvent() {
        return this.event;
    }
}
