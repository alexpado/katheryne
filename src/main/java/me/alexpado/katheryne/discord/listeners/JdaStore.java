package me.alexpado.katheryne.discord.listeners;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

@Service
public class JdaStore extends ListenerAdapter {

    @Nullable
    private JDA jda;

    public JdaStore() {
        this.jda = null;
    }

    @Override
    public void onGenericEvent(@NotNull GenericEvent event) {
        this.jda = event.getJDA();
    }

    public @Nullable JDA getJda() {
        return this.jda;
    }
}
