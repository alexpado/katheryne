package me.alexpado.katheryne.discord.listeners;

import me.alexpado.katheryne.genshin.entities.AdventurerGuild;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.genshin.repositories.AdventurerGuildRepository;
import me.alexpado.katheryne.genshin.repositories.TravelerRepository;
import net.dv8tion.jda.api.events.guild.update.GuildUpdateIconEvent;
import net.dv8tion.jda.api.events.guild.update.GuildUpdateNameEvent;
import net.dv8tion.jda.api.events.user.update.UserUpdateAvatarEvent;
import net.dv8tion.jda.api.events.user.update.UserUpdateDiscriminatorEvent;
import net.dv8tion.jda.api.events.user.update.UserUpdateNameEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;
import java.util.function.Consumer;

public class DatabaseUpdater extends ListenerAdapter {

    private final TravelerRepository        travelerRepository;
    private final AdventurerGuildRepository adventurerGuildRepository;

    public DatabaseUpdater(TravelerRepository travelerRepository, AdventurerGuildRepository adventurerGuildRepository) {
        this.travelerRepository        = travelerRepository;
        this.adventurerGuildRepository = adventurerGuildRepository;
    }

    private void applyTravelerEdit(long id, Consumer<Traveler> action) {
        Optional<Traveler> optionalTraveler = this.travelerRepository.findById(id);

        if (optionalTraveler.isEmpty()) {
            return; // Ignore
        }

        Traveler traveler = optionalTraveler.get();
        action.accept(traveler);
        this.travelerRepository.save(traveler);
    }

    private void applyGuildEdit(long id, Consumer<AdventurerGuild> action) {

        Optional<AdventurerGuild> optionalGuild = this.adventurerGuildRepository.findById(id);

        if (optionalGuild.isEmpty()) {
            return;
        }

        AdventurerGuild guild = optionalGuild.get();
        action.accept(guild);
        this.adventurerGuildRepository.save(guild);
    }

    @Override
    public void onUserUpdateName(@NotNull UserUpdateNameEvent event) {

        this.applyTravelerEdit(event.getUser().getIdLong(), traveler -> traveler.setName(event.getNewName()));
    }

    @Override
    public void onUserUpdateDiscriminator(@NotNull UserUpdateDiscriminatorEvent event) {

        this.applyTravelerEdit(event.getUser().getIdLong(), traveler -> traveler.setDiscriminator(event.getNewDiscriminator()));
    }

    @Override
    public void onUserUpdateAvatar(@NotNull UserUpdateAvatarEvent event) {

        this.applyTravelerEdit(event.getUser().getIdLong(), traveler -> traveler.setAvatar(event.getNewAvatarUrl()));
    }

    @Override
    public void onGuildUpdateIcon(@NotNull GuildUpdateIconEvent event) {
        this.applyGuildEdit(event.getGuild().getIdLong(), guild -> guild.setIcon(event.getNewIconUrl()));
    }

    @Override
    public void onGuildUpdateName(@NotNull GuildUpdateNameEvent event) {
        this.applyGuildEdit(event.getGuild().getIdLong(), guild -> guild.setName(event.getNewName()));
    }
}
