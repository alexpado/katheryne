package me.alexpado.katheryne.discord.interfaces;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.GenshinCoopApplication;
import me.alexpado.katheryne.genshin.entities.AdventurerGuild;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.genshin.repositories.AdventurerGuildRepository;
import me.alexpado.katheryne.genshin.repositories.TravelerRepository;

/**
 * This interface represents an {@link ICommandContext} for the {@link GenshinCoopApplication} bot.
 */
public interface IKatheryneContext extends ICommandContext, AutoCloseable {

    /**
     * Retrieves a {@link TravelerRepository} allowing interaction with the database for the {@link Traveler} entity.
     *
     * @return A {@link TravelerRepository}.
     */
    TravelerRepository getTravelerRepository();

    /**
     * Retrieves a {@link AdventurerGuildRepository} allowing interaction with the database for the {@link
     * AdventurerGuild} entity.
     *
     * @return A {@link AdventurerGuild}.
     */
    AdventurerGuildRepository getAdventurerGuildRepository();

    /**
     * Retrieves the current {@link AdventurerGuild} responsible for this {@link IKatheryneContext}'s creation.
     *
     * @return An {@link AdventurerGuild} instance.
     */
    AdventurerGuild getAdventurerGuild();

    /**
     * Retrieves the current {@link Traveler} responsible for this {@link IKatheryneContext}'s creation.
     *
     * @return A {@link Traveler} instance.
     */
    Traveler getTraveler();

}
