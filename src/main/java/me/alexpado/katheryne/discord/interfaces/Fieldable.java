package me.alexpado.katheryne.discord.interfaces;

import net.dv8tion.jda.api.entities.MessageEmbed;

public interface Fieldable {

    MessageEmbed.Field toField();

}
