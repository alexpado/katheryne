package me.alexpado.katheryne.discord;

import me.alexpado.katheryne.commissions.entities.Board;
import me.alexpado.katheryne.commissions.entities.Notification;
import me.alexpado.katheryne.discord.interfaces.IKatheryneContext;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import me.alexpado.katheryne.genshin.repositories.TravelerRepository;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Utils {

    /**
     * Retrieve the {@link Traveler}'s ID from the provided string.
     *
     * @param mention
     *         The string representing the mention.
     *
     * @return An {@link Optional} wrapping the ID. Empty is the provided string is not a mention.
     */
    public static Optional<Long> getMentionId(String mention) {

        // No Hades#9165 were pinged during this bugfix.
        // Thanks to SeijiMari#1020 for participating in this bugfix.
        Matcher matcher = Pattern.compile("<@!?[0-9]+>").matcher(mention);

        if (matcher.matches()) {
            int  size = mention.contains("!") ? 3 : 2;
            long id   = Long.parseLong(mention.substring(size, mention.length() - 1));
            return Optional.of(id);
        }

        return Optional.empty();
    }

    public static Optional<Long> getChannelId(String mention) {
        Matcher matcher = Pattern.compile("<#[0-9]+>").matcher(mention);
        Matcher digit   = Pattern.compile("[0-9]+").matcher(mention);

        if (matcher.matches()) {
            long id = Long.parseLong(mention.substring(2, mention.length() - 1));
            return Optional.of(id);
        } else if (digit.matches()) {
            return Optional.of(Long.parseLong(mention));
        }

        return Optional.empty();
    }

    public static Optional<TextChannel> getChannel(IKatheryneContext context, String mention) {

        Optional<Long> optionalId = getChannelId(mention);

        if (optionalId.isEmpty()) {
            return Optional.empty();
        }

        return Optional.ofNullable(context.getEvent().getGuild().getTextChannelById(optionalId.get()));

    }

    /**
     * Retrieve the {@link Traveler}'s {@link DiscordTag} from the provided string.
     *
     * @param tag
     *         The string representing the tag.
     *
     * @return An {@link Optional} wrapping the {@link DiscordTag}. Empty is the provided string is not a tag.
     */
    public static Optional<DiscordTag> getTag(String tag) {

        Matcher matcher = Pattern.compile(".+#[0-9]{4}").matcher(tag);

        if (matcher.matches()) {
            String[] tagPart = tag.split("#");
            return Optional.of(new DiscordTag(tagPart[0], tagPart[1]));
        }

        return Optional.empty();
    }

    /**
     * Convert the provided {@link Member} to a mention.
     * <p>
     * The main difference between this method and {@link Member#getAsMention()} is that this one include the
     * <code>!</code> character that is added when a mention is received, as seen in the {@link
     * Message#getContentRaw()} return value.
     * <p>
     * The reason that this character wasn't included in the {@link Member#getAsMention()} is unknown but still mess up
     * any comparison.
     *
     * @param member
     *         The {@link Member} to convert as mention.
     *
     * @return The mention string.
     */
    public static String toMention(Member member) {

        return String.format("<@!%s>", member.getId());
    }

    public static Optional<Traveler> retrieveTraveler(TravelerRepository repository, String mention) {

        Optional<Long>       optionalId  = Utils.getMentionId(mention);
        Optional<DiscordTag> optionalTag = Utils.getTag(mention);

        Optional<Traveler> optionalUser = Optional.empty();

        if (optionalId.isPresent()) {
            optionalUser = repository.findById(optionalId.get());
        } else if (optionalTag.isPresent()) {
            optionalUser = optionalTag.get().find(repository);
        }

        return optionalUser;
    }

    /**
     * Retrieve the source object as string or the string value if the source is null.
     *
     * @param source
     *         The source object from which the string will be retrieved.
     * @param ifNull
     *         The default value if the source is null.
     *
     * @return The source as string, or the default value.
     */
    public static String strIfNull(Object source, String ifNull) {

        if (source == null) {
            return ifNull;
        }
        return String.valueOf(source);
    }

    public static boolean hasOnePermission(@Nullable Member member, Permission... permissions) {
        if (member == null) {
            return false;
        }

        for (Permission permission : permissions) {
            if (member.hasPermission(permission)) {
                return true;
            }
        }

        return false;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean hasBasicPrivileges(IKatheryneContext context) {
        Member member = context.getEvent().getMember();

        return context.getTraveler().isAdmin() || Utils.hasOnePermission(member, Permission.MANAGE_SERVER, Permission.ADMINISTRATOR);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean hasBasicPrivileges(Interaction event, Traveler traveler) {
        Member member = event.getMember();

        return traveler.isAdmin() || Utils.hasOnePermission(member, Permission.MANAGE_SERVER, Permission.ADMINISTRATOR);
    }

    public static boolean hasTextPrivileges(TextChannel textChannel) {
        return textChannel.canTalk() && textChannel.getGuild().getSelfMember().hasPermission(textChannel, Permission.MESSAGE_EMBED_LINKS);
    }

    public static List<Notification> generateNotifications(Board board, ServerRegion serverRegion) {
        return ServerRegion.availableRegions(serverRegion).stream().map(region -> new Notification(board, region)).collect(Collectors.toList());
    }

}
