package me.alexpado.katheryne.discord;

import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.genshin.repositories.TravelerRepository;

import java.util.Optional;

public class DiscordTag {

    private final String name;
    private final String discriminator;

    public DiscordTag(String name, String discriminator) {

        this.name          = name;
        this.discriminator = discriminator;
    }

    public String getName() {

        return name;
    }

    public String getDiscriminator() {

        return discriminator;
    }

    public Optional<Traveler> find(TravelerRepository repository) {

        return repository.findByNameAndDiscriminator(this.name, this.discriminator);
    }
}
