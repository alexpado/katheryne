package me.alexpado.katheryne.discord;

import fr.alexpado.jda.DiscordBotImpl;
import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.annotations.Command;
import fr.alexpado.jda.services.commands.interfaces.*;
import me.alexpado.katheryne.genshin.entities.AdventurerGuild;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.genshin.repositories.AdventurerGuildRepository;
import me.alexpado.katheryne.genshin.repositories.TravelerRepository;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.ISnowflake;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.function.Function;

@Component
public class CommandHelper implements ICommandHelper {

    private final AdventurerGuildRepository adventurerGuildRepository;
    private final TravelerRepository        travelerRepository;

    @Value("${discord.prefix}")
    private String prefix;

    public CommandHelper(AdventurerGuildRepository adventurerGuildRepository, TravelerRepository travelerRepository) {

        this.adventurerGuildRepository = adventurerGuildRepository;
        this.travelerRepository        = travelerRepository;
    }

    /**
     * Called by {@link ICommandHandler}.
     *
     * Creates a new instance implementing {@link ICommandContext}. This will be then used by all commands that require
     * a command context.
     *
     * @param event
     *         The {@link MessageReceivedEvent} needing an {@link ICommandContext}.
     *
     * @return The {@link ICommandContext} implementation.
     */
    @Override
    public ICommandContext createContext(MessageReceivedEvent event) {

        // A command is being executed. Let's add our entities to the database if needed.
        Traveler        traveler = this.findByIdOrSave(event.getAuthor(), this.travelerRepository, Traveler::new);
        AdventurerGuild guild    = this.findByIdOrSave(event.getGuild(), this.adventurerGuildRepository, AdventurerGuild::new);

        return new KatheryneContext(event, this.travelerRepository, this.adventurerGuildRepository, traveler, guild);
    }

    private <T extends ISnowflake, U> U findByIdOrSave(T id, JpaRepository<U, Long> repository, Function<T, U> creator) {
        Optional<U> optionalEntity = repository.findById(id.getIdLong());
        return optionalEntity.orElseGet(() -> repository.save(creator.apply(id)));
    }

    /**
     * Retrieve the {@link EmbedBuilder} that may be used by {@link ICommand} to provide the same base across all your
     * bot.
     *
     * This is particularly useful when you want to add a title or footer to all {@link MessageEmbed}.
     *
     * @param event
     *         The {@link MessageReceivedEvent} to use to generate the {@link EmbedBuilder}.
     *
     * @return A {@link EmbedBuilder} template.
     */
    @Override
    public EmbedBuilder getVendorEmbed(@NotNull MessageReceivedEvent event) {
        return this.getVendorEmbed(event.getJDA())
                .setFooter(event.getAuthor().getName() + "#" + event.getAuthor().getDiscriminator(), event.getAuthor().getAvatarUrl());

    }

    /**
     * Retrieve the {@link EmbedBuilder} that may be used by {@link ICommand} to provide the same base across all your
     * bot.
     *
     * This is particularly useful when you want to add a title or footer to all {@link MessageEmbed}.
     *
     * @param jda
     *         The {@link JDA} to use to generate the {@link EmbedBuilder}.
     *
     * @return A {@link EmbedBuilder} template.
     */
    @Override
    public EmbedBuilder getVendorEmbed(@NotNull JDA jda) {
        EmbedBuilder builder = new EmbedBuilder();

        long   permission = Permission.getRaw(Permission.MESSAGE_SEND, Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_MANAGE);
        long   clientId   = jda.getSelfUser().getIdLong();
        String inviteLink = String.format(DiscordBotImpl.RAW_BOT_ADD_LINK, clientId, permission);

        builder.setAuthor("Click here to add me to your server !", inviteLink, jda.getSelfUser().getAvatarUrl());

        return builder;
    }

    /**
     * Called by {@link ICommandHandler}.
     *
     * Retrieve the command prefix to use with the provided {@link MessageReceivedEvent}.
     *
     * This method being called doesn't assure that the {@link MessageReceivedEvent} is associated with a command, but
     * will rather depends on the prefix provided.
     *
     * @param event
     *         The {@link MessageReceivedEvent}.
     *
     * @return The prefix to use to check if the {@link MessageReceivedEvent} can be handled as command.
     */
    @Override
    public String getApplicablePrefix(@NotNull MessageReceivedEvent event) {

        AdventurerGuild guild = this.findByIdOrSave(event.getGuild(), this.adventurerGuildRepository, AdventurerGuild::new);
        return Optional.ofNullable(guild.getPrefix()).orElse(this.prefix);
    }

    /**
     * Called by {@link ICommandHandler}.
     *
     * Retrieve the message to send to the user if his message started with the command prefix, but not {@link ICommand}
     * could be matched.
     *
     * @param event
     *         The {@link MessageReceivedEvent} that caused this command.
     * @param label
     *         The label of the command that wasn't found.
     *
     * @return The {@link MessageBuilder} to send to the user.
     */
    @Override
    public EmbedBuilder onCommandNotFound(@NotNull MessageReceivedEvent event, @NotNull String label) {

        EmbedBuilder embed = this.getVendorEmbed(event);
        embed.setDescription(String.format("Sorry Traveler, I couldn't find the command you asked for. Please check the `%shelp` command.", this.getApplicablePrefix(event)));
        return embed;
    }

    /**
     * Called by {@link ICommandHandler}.
     *
     * Retrieve the message to send to the user if his message could be matched with an {@link ICommand} but no method
     * annotated with {@link Command} could be matched.
     *
     * @param command
     *         The {@link ICommand} that was in use.
     * @param event
     *         The {@link MessageReceivedEvent} that caused this command.
     *
     * @return The {@link MessageBuilder} to send to the user.
     */
    @Override
    public EmbedBuilder onSyntaxError(@NotNull ICommand command, @NotNull MessageReceivedEvent event) {

        EmbedBuilder embed = this.getVendorEmbed(event);
        embed.setDescription(String.format("Sorry Traveler, you surely did a typo in your command. Please use the `%s%s help` command.", this.getApplicablePrefix(event), command.getMeta().getLabel()));
        return embed;
    }

    /**
     * Called by {@link ICommandHandler}.
     *
     * Retrieve the message to send to the user if a command encountered an error.
     *
     * @param event
     *         The {@link MessageReceivedEvent} that caused this command.
     * @param throwable
     *         The {@link Throwable} that was thrown.
     *
     * @return The {@link MessageBuilder} to send to the user.
     */
    @Override
    public EmbedBuilder onException(@NotNull MessageReceivedEvent event, @NotNull Throwable throwable) {

        EmbedBuilder embed = this.getVendorEmbed(event);

        embed.setTitle("Error... Rebooting...");
        embed.setDescription("Sorry Traveler, something not expected by the developer happened. But don't worry, he has been warned about this situation.");

        return embed;
    }

    /**
     * Called by {@link ICommandHandler}.
     * <p>
     * This method is called when the {@link IDiscordBot} doesn't have the permission to send a {@link Message} or an
     * {@link EmbedBuilder} in a channel.
     * <p>
     * This will happen after {@link #onCommandExecuted(ICommandEvent)}.
     *
     * @param event
     *         The {@link MessageReceivedEvent} that caused this command.
     */
    @Override
    public void onPermissionMissing(@NotNull MessageReceivedEvent event) {

    }

    /**
     * Called by {@link ICommandHandler}.
     *
     * This method is called right before an {@link ICommand} execution. You may want to use this to log executed
     * command for statistics purpose, or cancel a command based on {@link ICommandEvent} content.
     *
     * @param event
     *         The {@link ICommandEvent} containing information about the current command flow.
     */
    @Override
    public void onCommandExecuted(@NotNull ICommandEvent event) {

    }
}
