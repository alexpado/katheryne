package me.alexpado.katheryne.sockets.socket;

import me.alexpado.katheryne.sockets.socket.interfaces.ISocketable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Service
public class WebsocketBroadcaster {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebsocketBroadcaster.class);

    private final SimpMessagingTemplate socket;

    public WebsocketBroadcaster(SimpMessagingTemplate socket) {
        this.socket = socket;
    }

    @EventListener(ISocketable.class)
    public void onSocketableEvent(ISocketable socketable) {
        this.broadcast(socketable);
    }

    public void broadcast(ISocketable socketable) {
        LOGGER.debug("Sending broadcast on {}", socketable.getChannel());
        this.socket.convertAndSend(socketable.getChannel(), socketable.getSocketData().toString());
    }

    @EventListener(SessionConnectedEvent.class)
    public void onWebsocketSessionOpened(SessionConnectedEvent event) {
        SimpMessageHeaderAccessor accessor = SimpMessageHeaderAccessor.wrap(event.getMessage());
        LOGGER.info("[{}] Websocket session opened.", accessor.getSessionId());
    }

    @EventListener(SessionDisconnectEvent.class)
    public void onWebsocketSessionClosed(SessionDisconnectEvent event) {
        LOGGER.debug("[{}] Websocket session closed.", event.getSessionId());
    }

}
