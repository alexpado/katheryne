package me.alexpado.katheryne.sockets.socket.interfaces;

import org.json.JSONObject;

public interface ISocketable {

    /**
     * Retrieve the JSON data to be sent through the socket.
     *
     * @return The JSON data
     */
    JSONObject getSocketData();

    /**
     * Retrieve the channel to which data will be sent.
     *
     * @return The channel
     */
    String getChannel();

}
