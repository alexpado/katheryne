package me.alexpado.katheryne.slash.commands;

import fr.alexpado.jda.interactions.annotations.Choice;
import fr.alexpado.jda.interactions.annotations.Interact;
import fr.alexpado.jda.interactions.annotations.Option;
import fr.alexpado.jda.interactions.annotations.Param;
import fr.alexpado.jda.interactions.entities.responses.SimpleInteractionResponse;
import fr.alexpado.jda.interactions.enums.InteractionType;
import fr.alexpado.jda.interactions.interfaces.interactions.InteractionResponse;
import me.alexpado.katheryne.discord.DiscordTag;
import me.alexpado.katheryne.discord.Utils;
import me.alexpado.katheryne.genshin.entities.Profile;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import me.alexpado.katheryne.genshin.repositories.ProfileRepository;
import me.alexpado.katheryne.genshin.repositories.TravelerRepository;
import me.alexpado.katheryne.messages.profiles.*;
import me.alexpado.katheryne.messages.traveler.TravelerProfileNoteDefinedEmbed;
import me.alexpado.katheryne.slash.InteractionClass;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.interactions.Interaction;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

@Component
@InteractionClass
public class ProfileSlashCommand {

    private final ProfileRepository  profileRepository;
    private final TravelerRepository travelerRepository;

    public ProfileSlashCommand(ProfileRepository profileRepository, TravelerRepository travelerRepository) {
        this.profileRepository  = profileRepository;
        this.travelerRepository = travelerRepository;
    }

    // <editor-fold desc="- Private Helper Methods">
    private EmbedBuilder showProfile(Interaction event, Supplier<Optional<Traveler>> travelerSupplier) {
        Optional<Traveler> optionalTraveler = travelerSupplier.get();

        if (optionalTraveler.isEmpty()) {
            return new ProfileNotFoundEmbed(event);
        }

        Traveler traveler = optionalTraveler.get();
        return traveler.asEmbed(event, this.profileRepository);
    }

    private Profile getProfile(Traveler traveler, ServerRegion region) {
        return this.profileRepository.findByTravelerAndRegion(traveler, region).orElseGet(() -> new Profile(traveler, region));
    }
    // </editor-fold>

    // <editor-fold desc="@ profile/update [region] (wl,uid)">
    @Interact(
            name = "profile/update",
            description = "Update your profile visible to every Travelers.",
            type = InteractionType.SLASH,
            options = {
                    @Option(
                            name = "region",
                            description = "The profile's region you want to update.",
                            required = true,
                            type = OptionType.STRING,
                            choices = {
                                    @Choice(id = "NA", display = "na"),
                                    @Choice(id = "EU", display = "eu"),
                                    @Choice(id = "ASIA", display = "asia"),
                                    @Choice(id = "SAR", display = "sar")
                            }
                    ),
                    @Option(
                            name = "wl",
                            description = "Your profile's world level.",
                            type = OptionType.INTEGER,
                            choices = {
                                    @Choice(id = "1", display = "1"),
                                    @Choice(id = "2", display = "2"),
                                    @Choice(id = "3", display = "3"),
                                    @Choice(id = "4", display = "4"),
                                    @Choice(id = "5", display = "5"),
                                    @Choice(id = "6", display = "6"),
                                    @Choice(id = "7", display = "7"),
                                    @Choice(id = "8", display = "8")
                            }
                    ),
                    @Option(
                            name = "uid",
                            description = "Your profile's uid",
                            type = OptionType.STRING
                    )
            }
    )
    // </editor-fold>
    public InteractionResponse updateProfileProperties(Interaction event, Traveler traveler, @Param("region") String regionOpt, @Param("wl") Long wlOptLong, @Param("uid") String uidOpt) {

        ServerRegion region  = ServerRegion.valueOf(regionOpt);
        Profile      profile = this.getProfile(traveler, region);

        if (wlOptLong != null) {
            String  worldLevelString  = wlOptLong.toString();
            Integer worldLevelInteger = Integer.parseInt(worldLevelString);
            profile.setWorldLevel(worldLevelInteger);
        }

        if (uidOpt != null) {
            if (!region.isUidCompatible(uidOpt)) {
                return new SimpleInteractionResponse(new WrongUIDEmbed(event));
            }
            profile.setUid(uidOpt);
        }

        this.profileRepository.save(profile);

        boolean uidUpdated        = uidOpt != null;
        boolean worldLevelUpdated = wlOptLong != null;

        if (worldLevelUpdated && uidUpdated) {
            return new SimpleInteractionResponse(new FullUpdatedEmbed(event, region));
        } else if (worldLevelUpdated) {
            return new SimpleInteractionResponse(new WorldLevelUpdatedEmbed(event, region));
        } else if (uidUpdated) {
            return new SimpleInteractionResponse(new UIDUpdatedEmbed(event, region));
        } else {
            return new SimpleInteractionResponse(new NothingUpdatedEmbed(event));
        }
    }

    // <editor-fold desc="@ profile/main [region]">
    @Interact(
            name = "profile/main",
            description = "Change your main profile.",
            type = InteractionType.SLASH,
            options = {
                    @Option(
                            name = "region",
                            description = "The profile's region you want to switch to.",
                            required = true,
                            type = OptionType.STRING,
                            choices = {
                                    @Choice(id = "NA", display = "na"),
                                    @Choice(id = "EU", display = "eu"),
                                    @Choice(id = "ASIA", display = "asia"),
                                    @Choice(id = "SAR", display = "sar")
                            }
                    ),
            }
    )
    // </editor-fold>
    public InteractionResponse changeMainProfile(Interaction event, Traveler traveler, @Param("region") String regionOpt) {

        ServerRegion  region   = ServerRegion.valueOf(regionOpt);
        List<Profile> profiles = this.profileRepository.findAllByTraveler(traveler);

        // Foolproof check.
        if (profiles.stream().noneMatch(profile -> profile.getRegion() == region)) {
            return new SimpleInteractionResponse(new NoMatchedProfileEmbed(event));
        }

        profiles.forEach(profile -> profile.setMain(profile.getRegion() == region));
        this.profileRepository.saveAll(profiles);
        return new SimpleInteractionResponse(new MainProfileSetEmbed(event, region));
    }

    // <editor-fold desc="@ profile/view">
    @Interact(
            name = "profile/view",
            description = "View your own Traveler's profile.",
            type = InteractionType.SLASH
    )
    // </editor-fold>
    public InteractionResponse viewProfile(Interaction event, Traveler traveler) {
        return new SimpleInteractionResponse(traveler.asEmbed(event, this.profileRepository));
    }

    // <editor-fold desc="@ profile/search <traveler,tag,uid>">
    @Interact(
            name = "profile/search",
            description = "Search a Traveler's profile.",
            type = InteractionType.SLASH,
            options = {
                    @Option(
                            name = "traveler",
                            description = "The server member",
                            type = OptionType.USER
                    ),
                    @Option(
                            name = "tag",
                            description = "A discord tag",
                            type = OptionType.STRING
                    ),
                    @Option(
                            name = "uid",
                            description = "A Traveler's UID",
                            type = OptionType.INTEGER
                    )
            }
    )
    // </editor-fold>
    public InteractionResponse searchProfile(Interaction event, @Param("traveler") User userOpt, @Param("tag") String tagOpt, @Param("uid") String uidOpt) {

        long parameterCount = Stream.of(userOpt, tagOpt, uidOpt).filter(Objects::nonNull).count();

        if (parameterCount == 0) {
            return new SimpleInteractionResponse(new NotEnoughSearchOptionsEmbed(event));
        } else if (parameterCount > 1) {
            return new SimpleInteractionResponse(new TooMuchSearchOptionsEmbed(event));
        }

        if (userOpt != null) {
            return new SimpleInteractionResponse(this.showProfile(event, () -> this.travelerRepository.findById(userOpt.getIdLong())));
        } else if (tagOpt != null) {

            Optional<DiscordTag> tag = Utils.getTag(tagOpt);

            return tag.map(discordTag -> new SimpleInteractionResponse(this.showProfile(event, () -> discordTag.find(this.travelerRepository))))
                    .orElseGet(() -> new SimpleInteractionResponse(new ProfileNotFoundEmbed(event)));

        } else {
            List<Profile> allByUid = this.profileRepository.findAllByUid(uidOpt);
            if (allByUid.size() > 1) {
                return new SimpleInteractionResponse(new TooMuchProfileEmbed(event));
            } else if (allByUid.isEmpty()) {
                return new SimpleInteractionResponse(new ProfileNotFoundEmbed(event));
            } else {
                Profile profile = allByUid.get(0);
                return new SimpleInteractionResponse(profile.getTraveler().asEmbed(event, this.profileRepository));
            }
        }
    }

    // <editor-fold desc="@ profile/public-uid [visible]">
    @Interact(
            name = "profile/public-uid",
            description = "Change your UID visibility.",
            type = InteractionType.SLASH,
            options = {
                    @Option(
                            name = "visible",
                            description = "Visibility state of your UID.",
                            type = OptionType.BOOLEAN,
                            required = true
                    )
            }
    )
    // </editor-fold>
    public InteractionResponse changeUidVisibility(Interaction event, Traveler traveler, @Param("visible") boolean visible) {

        traveler.setUidHidden(!visible);
        this.travelerRepository.save(traveler);
        return new SimpleInteractionResponse(new UIDVisibilityEmbed(event, traveler));
    }

    // <editor-fold desc="@ traveler/note/set">
    @Interact(
            name = "traveler/note/set",
            description = "Add a custom note to your profile",
            type = InteractionType.SLASH,
            options = {
                    @Option(
                            name = "note",
                            description = "The note to add to your profile",
                            type = OptionType.STRING,
                            required = true
                    )
            }
    )
    // </editor-fold>
    public InteractionResponse changeTravelerNote(Interaction event, Traveler traveler, @Param("note") String note) {

        traveler.setNote(note);
        this.travelerRepository.save(traveler);
        return new SimpleInteractionResponse(new TravelerProfileNoteDefinedEmbed(event));
    }

    // <editor-fold desc="@ traveler/note/remove">
    @Interact(
            name = "traveler/note/remove",
            description = "Remove the custom note from profile",
            type = InteractionType.SLASH
    )
    // </editor-fold>
    public InteractionResponse removeTravelerNote(Interaction event, Traveler traveler) {

        traveler.setNote(null);
        this.travelerRepository.save(traveler);
        return new SimpleInteractionResponse(new TravelerProfileNoteDefinedEmbed(event));
    }

}
