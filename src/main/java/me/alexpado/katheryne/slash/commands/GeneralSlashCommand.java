package me.alexpado.katheryne.slash.commands;


import fr.alexpado.jda.interactions.annotations.Interact;
import fr.alexpado.jda.interactions.entities.responses.SimpleInteractionResponse;
import fr.alexpado.jda.interactions.enums.InteractionType;
import fr.alexpado.jda.interactions.interfaces.interactions.InteractionResponse;
import me.alexpado.katheryne.messages.changelogs.ChangelogEmbed;
import me.alexpado.katheryne.messages.helps.AboutEmbed;
import me.alexpado.katheryne.slash.InteractionClass;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;
import org.springframework.stereotype.Component;

@Component
@InteractionClass
public class GeneralSlashCommand {

    public GeneralSlashCommand() {

    }

    // <editor-fold desc="@ about">
    @Interact(
            name = "about",
            description = "About the bot",
            type = InteractionType.SLASH
    )
    // </editor-fold>
    public InteractionResponse displayAbout(Interaction event) {
        return new SimpleInteractionResponse(new AboutEmbed(event));
    }

    // <editor-fold desc="@ changelog">
    @Interact(
            name = "changelog",
            description = "Check what's new in the current release of the bot !",
            type = InteractionType.SLASH
    )
    // </editor-fold>
    public InteractionResponse changelog(Interaction event) {
        return new SimpleInteractionResponse(new ChangelogEmbed(event));
    }

}
