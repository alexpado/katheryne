package me.alexpado.katheryne.slash.commands;

import fr.alexpado.jda.interactions.annotations.Choice;
import fr.alexpado.jda.interactions.annotations.Interact;
import fr.alexpado.jda.interactions.annotations.Option;
import fr.alexpado.jda.interactions.annotations.Param;
import fr.alexpado.jda.interactions.entities.responses.SimpleInteractionResponse;
import fr.alexpado.jda.interactions.enums.InteractionType;
import fr.alexpado.jda.interactions.interfaces.interactions.InteractionResponse;
import me.alexpado.katheryne.commissions.entities.Commission;
import me.alexpado.katheryne.commissions.enums.CommissionType;
import me.alexpado.katheryne.commissions.events.CommissionUpdateEvent;
import me.alexpado.katheryne.commissions.repositories.CommissionRepository;
import me.alexpado.katheryne.genshin.entities.Profile;
import me.alexpado.katheryne.genshin.entities.Restriction;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import me.alexpado.katheryne.genshin.repositories.ProfileRepository;
import me.alexpado.katheryne.genshin.repositories.RestrictionRepository;
import me.alexpado.katheryne.messages.commissions.*;
import me.alexpado.katheryne.slash.InteractionClass;
import net.dv8tion.jda.api.interactions.Interaction;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;

@Component
@InteractionClass
public class CommissionSlashCommand {

    private final CommissionRepository      commissionRepository;
    private final ProfileRepository         profileRepository;
    private final RestrictionRepository     restrictionRepository;
    private final ApplicationEventPublisher publisher;

    public CommissionSlashCommand(CommissionRepository commissionRepository, ProfileRepository profileRepository, RestrictionRepository restrictionRepository, ApplicationEventPublisher publisher) {

        this.commissionRepository  = commissionRepository;
        this.profileRepository     = profileRepository;
        this.restrictionRepository = restrictionRepository;
        this.publisher             = publisher;
    }

    // <editor-fold desc="@ commission/create">
    @Interact(
            name = "commission/create",
            description = "Create a commission",
            type = InteractionType.SLASH,
            options = {
                    @Option(
                            name = "type",
                            description = "Type of the commission to create",
                            type = OptionType.STRING,
                            required = true,
                            choices = {
                                    @Choice(id = "BOSS", display = "Boss"),
                                    @Choice(id = "BOUNTY", display = "Bounties"),
                                    @Choice(id = "DOMAIN", display = "Domains"),
                                    @Choice(id = "GRINDING", display = "Grinding"),
                                    @Choice(id = "MOB", display = "Mob Farming"),
                                    @Choice(id = "RESOURCE", display = "Resources Farming"),
                                    @Choice(id = "CHALLENGE", display = "Challenge"),
                                    @Choice(id = "EVENT", display = "Event"),
                                    @Choice(id = "WEI", display = "Wei"),
                                    @Choice(id = "TEAPOT", display = "Serenitea pot"),
                                    @Choice(id = "OTHER", display = "Other")
                            }
                    ),
                    @Option(
                            name = "message",
                            description = "The message describing your commission",
                            type = OptionType.STRING,
                            required = true
                    ),
                    @Option(
                            name = "region",
                            description = "Region where the commission will be created (default to your main profile's region)",
                            type = OptionType.STRING,
                            choices = {
                                    @Choice(id = "NA", display = "na"),
                                    @Choice(id = "EU", display = "eu"),
                                    @Choice(id = "ASIA", display = "asia"),
                                    @Choice(id = "SAR", display = "sar")
                            }
                    )
            }
    )
    // </editor-fold>
    public InteractionResponse createNewCommission(Interaction event, Traveler traveler, @Param("region") String regionParam, @Param("type") String typeParam, @Param("message") String message) {

        CommissionType type = CommissionType.valueOf(typeParam);

        Optional<Restriction> optionalRestriction = traveler.getRestriction(this.restrictionRepository);

        if (optionalRestriction.isPresent()) {
            return new SimpleInteractionResponse(new TravelerRestrictedEmbed(event, optionalRestriction.get()));
        }

        Optional<Profile>      optionalProfile;
        Optional<ServerRegion> optionalRegion = ServerRegion.fromName(regionParam, true);

        if (optionalRegion.isPresent()) {
            optionalProfile = this.profileRepository.findByTravelerAndRegion(traveler, optionalRegion.get());
        } else {
            optionalProfile = this.profileRepository.findByTravelerAndMainIsTrue(traveler);
        }

        if (optionalProfile.isEmpty() || !optionalProfile.get().isProfileMinimumFilled()) {
            return new SimpleInteractionResponse(new FillProfileErrorEmbed(event));
        }

        if (this.commissionRepository.findByProfileTravelerIdAndClosedIsFalse(traveler.getId()).isPresent()) {
            return new SimpleInteractionResponse(new CommissionAlreadyOpenedEmbed(event));
        }

        if (message.length() > 255) {
            return new SimpleInteractionResponse(new CommissionMessageTooLongEmbed(event));
        }

        Commission commission = Commission.create(optionalProfile.get(), type, message);
        this.commissionRepository.save(commission);

        this.publisher.publishEvent(new CommissionUpdateEvent(this, commission));
        return new SimpleInteractionResponse(new CommissionOpenedEmbed(event, traveler.isUidHidden()));
    }

    // <editor-fold desc="@ commission/close">
    @Interact(
            name = "commission/close",
            description = "Close your currently opened commission.",
            type = InteractionType.SLASH
    )
    // </editor-fold>
    public InteractionResponse closeCommission(Interaction event, Traveler traveler) {

        Optional<Commission> optionalCommission = this.commissionRepository.findByProfileTravelerIdAndClosedIsFalse(traveler.getId());

        if (optionalCommission.isEmpty()) {
            return new SimpleInteractionResponse(new CommissionNotOpenedEmbed(event));
        }

        Commission commission = optionalCommission.get();

        commission.setClosedAt(LocalDateTime.now());
        commission.setClosed(true);

        this.commissionRepository.save(commission);
        this.publisher.publishEvent(new CommissionUpdateEvent(this, commission));
        return new SimpleInteractionResponse(new CommissionClosedEmbed(event));
    }

}
