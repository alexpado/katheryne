package me.alexpado.katheryne.slash.commands;

import fr.alexpado.jda.interactions.annotations.Choice;
import fr.alexpado.jda.interactions.annotations.Interact;
import fr.alexpado.jda.interactions.annotations.Option;
import fr.alexpado.jda.interactions.annotations.Param;
import fr.alexpado.jda.interactions.entities.responses.SimpleInteractionResponse;
import fr.alexpado.jda.interactions.enums.InteractionType;
import fr.alexpado.jda.interactions.enums.SlashTarget;
import fr.alexpado.jda.interactions.interfaces.interactions.InteractionResponse;
import me.alexpado.katheryne.commissions.entities.Board;
import me.alexpado.katheryne.commissions.entities.Notification;
import me.alexpado.katheryne.commissions.repositories.BoardRepository;
import me.alexpado.katheryne.commissions.repositories.NotificationRepository;
import me.alexpado.katheryne.discord.Utils;
import me.alexpado.katheryne.genshin.entities.AdventurerGuild;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import me.alexpado.katheryne.messages.EmbedException;
import me.alexpado.katheryne.messages.base.NotEnoughPermissionEmbed;
import me.alexpado.katheryne.messages.board.*;
import me.alexpado.katheryne.messages.guild.WrongChannelMentionEmbed;
import me.alexpado.katheryne.messages.profiles.WrongServerRegionEmbed;
import me.alexpado.katheryne.slash.InteractionClass;
import me.alexpado.katheryne.slash.KatheryneSlash;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.interactions.Interaction;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

@Component
@InteractionClass
public class BoardSlashCommand {

    private final BoardRepository        boardRepository;
    private final NotificationRepository notificationRepository;

    public BoardSlashCommand(BoardRepository boardRepository, NotificationRepository notificationRepository) {
        this.boardRepository        = boardRepository;
        this.notificationRepository = notificationRepository;
    }

    // <editor-fold desc="- Private Helper Methods">
    private Optional<Board> getBoardFromChannel(AdventurerGuild guild, GuildChannel channel) {
        long guildId   = guild.getId();
        long channelId = channel.getIdLong();
        return this.boardRepository.findByGuildIdAndChannelId(guildId, channelId);
    }

    private Board requireBoard(Interaction event, AdventurerGuild guild, GuildChannel channel) {
        return this.getBoardFromChannel(guild, channel).orElseThrow(() -> new EmbedException(new BoardNotFoundEmbed(event)));
    }

    private void changeNotification(Interaction event, AdventurerGuild guild, ServerRegion region, GuildChannel channel, Consumer<Notification> consumer) {

        Board board = this.requireBoard(event, guild, channel);

        if (!board.isRegionCompatible(region)) {
            throw new EmbedException(new BoardNotificationUnmodifiableEmbed(event));
        }

        List<Notification> notifications = this.notificationRepository.findAllByBoardId(board.getId());
        notifications.forEach(consumer);
        this.notificationRepository.saveAll(notifications);
    }

    private ServerRegion getRegion(Interaction event, String region) {
        return ServerRegion.fromName(region, true).orElseThrow(() -> new EmbedException(new WrongServerRegionEmbed(event, false)));
    }

    private ServerRegion getRegion(String region) {
        return ServerRegion.fromName(region, false).orElse(ServerRegion.ALL);
    }

    private TextChannel asTextChannel(Interaction event, GuildChannel channel) {
        if (channel.getType() != ChannelType.TEXT) {
            throw new EmbedException(new WrongChannelMentionEmbed(event));
        }

        TextChannel textChannel = (TextChannel) channel;

        if (!Utils.hasTextPrivileges(textChannel)) {
            throw new EmbedException(new NeedMorePermissionEmbed(event));
        }

        return textChannel;
    }
    // </editor-fold>

    // <editor-fold desc="@ board/create [channel] <region>">
    @Interact(
            name = "board/create",
            description = "Create a new commission board",
            type = InteractionType.SLASH,
            target = SlashTarget.GUILD,
            options = {
                    @Option(
                            name = "channel",
                            description = "Channel into which the board will be created",
                            type = OptionType.CHANNEL,
                            required = true
                    ),
                    @Option(
                            name = "region",
                            description = "The region that will be used with the board (default ALL)",
                            type = OptionType.STRING,
                            choices = {
                                    @Choice(id = "ALL", display = "Use all region"),
                                    @Choice(id = "NA", display = "na"),
                                    @Choice(id = "EU", display = "eu"),
                                    @Choice(id = "ASIA", display = "asia"),
                                    @Choice(id = "SAR", display = "sar")
                            }
                    )
            }
    )
    // </editor-fold>
    public InteractionResponse createServerBoard(Interaction event, Traveler traveler, AdventurerGuild guild, @Param("channel") GuildChannel channel, @Param("region") String regionOpt) {

        KatheryneSlash.requireAdmin(event, traveler);
        TextChannel textChannel = this.asTextChannel(event, channel);

        ServerRegion    region        = this.getRegion(regionOpt);
        Optional<Board> optionalBoard = this.getBoardFromChannel(guild, textChannel);

        if (optionalBoard.isPresent()) {
            return new SimpleInteractionResponse(new BoardAlreadyExistsEmbed(event));
        }

        Board              board         = this.boardRepository.save(new Board(guild, region, textChannel.getIdLong()));
        List<Notification> notifications = Utils.generateNotifications(board, region);

        this.notificationRepository.saveAll(notifications);
        return new SimpleInteractionResponse(new BoardAddedEmbed(event));
    }

    // <editor-fold desc="@ board/remove [channel]">
    @Interact(
            name = "board/remove",
            description = "Remove a commission board",
            type = InteractionType.SLASH,
            target = SlashTarget.GUILD,
            options = {
                    @Option(
                            name = "channel",
                            description = "Channel from which the board will be deleted",
                            type = OptionType.CHANNEL,
                            required = true
                    )
            }
    )
    // </editor-fold>
    public InteractionResponse removeServerBoard(Interaction event, Traveler traveler, AdventurerGuild guild, @Param("channel") GuildChannel channel) {

        KatheryneSlash.requireAdmin(event, traveler);

        Board              board         = this.requireBoard(event, guild, channel);
        List<Notification> notifications = this.notificationRepository.findAllByBoardId(board.getId());

        // Removing the board message
        if (board.getMessageId() != null) {
            Guild       discordGuild = Objects.requireNonNull(event.getGuild());
            TextChannel boardChannel = Optional.ofNullable(discordGuild.getTextChannelById(board.getChannelId())).orElseThrow(() -> new EmbedException(new BoardInaccessibleEmbed(event)));
            boardChannel.deleteMessageById(board.getMessageId()).complete();
        }

        this.notificationRepository.deleteAll(notifications);
        this.boardRepository.delete(board);
        return new SimpleInteractionResponse(new BoardRemovedEmbed(event));
    }

    // <editor-fold desc="@ board/details [channel]">
    @Interact(
            name = "board/details",
            description = "Show details about a specific commission board",
            type = InteractionType.SLASH,
            target = SlashTarget.GUILD,
            options = {
                    @Option(
                            name = "channel",
                            description = "Channel from which the board will be selected",
                            type = OptionType.CHANNEL,
                            required = true
                    )
            }
    )
    // </editor-fold>
    public InteractionResponse showBoardDetails(Interaction event, Traveler traveler, AdventurerGuild guild, @Param("channel") GuildChannel channel) {

        KatheryneSlash.requireAdmin(event, traveler);

        Board              board         = this.requireBoard(event, guild, channel);
        List<Notification> notifications = this.notificationRepository.findAllByBoardId(board.getId());
        return new SimpleInteractionResponse(new BoardDetailsEmbed(event, board, notifications));
    }

    // <editor-fold desc="@ board/status">
    @Interact(
            name = "board/status",
            description = "(BOT ADMIN) Show every board status.",
            type = InteractionType.SLASH,
            defer = true
    )
    // </editor-fold>
    public InteractionResponse showBoardStatus(Interaction event, Traveler traveler) {

        if (!traveler.isAdmin()) {
            return new SimpleInteractionResponse(new NotEnoughPermissionEmbed(event));
        }

        List<Board> boards        = this.boardRepository.findAll();
        String      statusValid   = "\uD83D\uDFE2";
        String      statusInvalid = "\uD83D\uDD34";
        String      statusUnknown = "\uD83D\uDFE4";

        EmbedBuilder builder = new EmbedBuilder();
        builder.setTitle("Board Status");
        builder.setDescription("Found: " + statusValid + " • Not Found: " + statusInvalid + " • Can't check " + statusUnknown);

        String boardTitleFormat = "%s • Board ID %s";
        String boardFormat      = "Guild: %s • Channel: %s";

        for (Board board : boards) {
            AdventurerGuild adventurerGuild = board.getGuild();
            Guild           guild           = event.getJDA().getGuildById(adventurerGuild.getId());

            if (guild == null) {
                builder.addField(String.format(boardTitleFormat, adventurerGuild.getName(), board.getId()), String.format(boardFormat, statusInvalid, statusUnknown, statusUnknown), false);
                continue;
            }

            TextChannel channel = guild.getTextChannelById(board.getChannelId());

            if (channel == null) {
                builder.addField(String.format(boardTitleFormat, adventurerGuild.getName(), board.getId()), String.format(boardFormat, statusValid, statusInvalid, statusUnknown), false);
                continue;
            }

            builder.addField(String.format(boardTitleFormat, adventurerGuild.getName(), board.getId()), String.format(boardFormat, statusValid, statusValid), false);
        }

        return new SimpleInteractionResponse(builder);
    }

    // <editor-fold desc="@ notification/update [channel, region]">
    @Interact(
            name = "notification/update",
            description = "Change the message sent when a commission is posted",
            type = InteractionType.SLASH,
            target = SlashTarget.GUILD,
            options = {
                    @Option(
                            name = "channel",
                            description = "Channel from which the board will be selected",
                            type = OptionType.CHANNEL,
                            required = true
                    ),
                    @Option(
                            name = "region",
                            description = "The region from which the notification will be triggered",
                            type = OptionType.STRING,
                            required = true,
                            choices = {
                                    @Choice(id = "NA", display = "na"),
                                    @Choice(id = "EU", display = "eu"),
                                    @Choice(id = "ASIA", display = "asia"),
                                    @Choice(id = "SAR", display = "sar")
                            }
                    ),
                    @Option(
                            name = "message",
                            description = "Message for the notification",
                            type = OptionType.STRING,
                            required = true
                    )
            }
    )
    // </editor-fold>
    public InteractionResponse updateNotification(Interaction event, Traveler traveler, AdventurerGuild guild, @Param("channel") GuildChannel channel, @Param("region") String regionOpt, @Param("message") String message) {

        KatheryneSlash.requireAdmin(event, traveler);
        ServerRegion region = this.getRegion(event, regionOpt);

        this.changeNotification(event, guild, region, channel, notification -> {
            if (notification.isRegionCompatible(region)) {
                notification.setContent(message);
            }
        });

        return new SimpleInteractionResponse(new BoardUpdatedEmbed(event));
    }

    // <editor-fold desc="@ notification/trigger [channel, region]">
    @Interact(
            name = "notification/trigger",
            description = "Trigger a commission notification to test your settings",
            type = InteractionType.SLASH,
            target = SlashTarget.GUILD,
            options = {
                    @Option(
                            name = "channel",
                            description = "Channel from which the board will be selected",
                            type = OptionType.CHANNEL,
                            required = true
                    ),
                    @Option(
                            name = "region",
                            description = "The region from which the notification will be triggered",
                            type = OptionType.STRING,
                            required = true,
                            choices = {
                                    @Choice(id = "NA", display = "na"),
                                    @Choice(id = "EU", display = "eu"),
                                    @Choice(id = "ASIA", display = "asia"),
                                    @Choice(id = "SAR", display = "sar")
                            }
                    )
            }
    )
    // </editor-fold>
    public InteractionResponse triggerNotification(Interaction event, Traveler traveler, AdventurerGuild guild, @Param("channel") GuildChannel channel, @Param("region") String regionOpt) {

        KatheryneSlash.requireAdmin(event, traveler);

        ServerRegion region = this.getRegion(event, regionOpt);
        Board        board  = this.requireBoard(event, guild, channel);

        this.changeNotification(event, guild, region, channel, notification -> {
            if (notification.isRegionCompatible(region)) {
                notification.setEnabled(true);
            }
        });

        board.setNotifiable(true);
        this.boardRepository.save(board);
        return new SimpleInteractionResponse(new BoardNotificationTriggeredEmbed(event));
    }

}
