package me.alexpado.katheryne.slash;

import fr.alexpado.jda.interactions.entities.DispatchEvent;
import fr.alexpado.jda.interactions.interfaces.ExecutableItem;
import fr.alexpado.jda.interactions.interfaces.interactions.InteractionErrorHandler;
import fr.alexpado.jda.interactions.interfaces.interactions.InteractionItem;
import fr.alexpado.jda.interactions.interfaces.interactions.InteractionManager;
import io.sentry.Sentry;
import io.sentry.SentryLevel;
import me.alexpado.katheryne.Katheryne;
import me.alexpado.katheryne.discord.Utils;
import me.alexpado.katheryne.genshin.entities.AdventurerGuild;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.genshin.repositories.AdventurerGuildRepository;
import me.alexpado.katheryne.genshin.repositories.TravelerRepository;
import me.alexpado.katheryne.messages.EmbedException;
import me.alexpado.katheryne.messages.base.NotEnoughPermissionEmbed;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.Interaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class KatheryneSlash extends ListenerAdapter implements InteractionErrorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(KatheryneSlash.class);

    private final AdventurerGuildRepository adventurerGuildRepository;
    private final TravelerRepository        travelerRepository;

    @Value("${discord.devmode:false}")
    private boolean devmode;

    private String notice;

    public KatheryneSlash(AdventurerGuildRepository adventurerGuildRepository, TravelerRepository travelerRepository) {

        this.adventurerGuildRepository = adventurerGuildRepository;
        this.travelerRepository        = travelerRepository;
    }

    public static void requireAdmin(Interaction event, Traveler traveler) {
        if (!Utils.hasBasicPrivileges(event, traveler)) {
            throw new EmbedException(new NotEnoughPermissionEmbed(event));
        }
    }

    public void prepare(ReadyEvent event, ApplicationContext context) {

        InteractionManager manager = InteractionManager.using(event.getJDA(), this);

        manager.registerMapping(AdventurerGuild.class, this::getAdventurerGuild);
        manager.registerMapping(Traveler.class, this::getTraveler);
        manager.registerMapping(KatheryneSlash.class, interaction -> this);
        manager.registerMapping(Interaction.class, interaction -> interaction);

        for (String beanName : context.getBeanDefinitionNames()) {
            Object bean = context.getBean(beanName);
            if (bean.getClass().isAnnotationPresent(InteractionClass.class)) {
                LOGGER.info("Found command bean {} -- {}", beanName, bean.getClass());
                manager.registerInteraction(bean);
            }
        }

        if (this.devmode) {
            LOGGER.info("Running in developer mode !");
            Guild adventurerGuild = event.getJDA().getGuildById(Katheryne.ADVENTURER_GUILD_ID);

            if (adventurerGuild == null) {
                LOGGER.warn("Unable to find Adventurer's guild.");
            } else {
                manager.build(adventurerGuild).queue();
            }
        } else {
            LOGGER.info("Running in normal mode !");
            manager.build(event.getJDA()).queue();
        }
    }

    private AdventurerGuild getAdventurerGuild(Interaction event) {

        Guild                     guild                   = Objects.requireNonNull(event.getGuild());
        Optional<AdventurerGuild> optionalAdventurerGuild = this.adventurerGuildRepository.findById(guild.getIdLong());
        AdventurerGuild           adventurerGuild;

        if (optionalAdventurerGuild.isPresent()) {
            adventurerGuild = optionalAdventurerGuild.get();
            adventurerGuild.setName(guild.getName());
            adventurerGuild.setIcon(guild.getIconUrl());
        } else {
            adventurerGuild = new AdventurerGuild(guild);
        }

        return this.adventurerGuildRepository.save(adventurerGuild);
    }

    private Traveler getTraveler(Interaction event) {

        Optional<Traveler> optionalTraveler = this.travelerRepository.findById(event.getUser().getIdLong());
        Traveler           traveler;

        if (optionalTraveler.isPresent()) {
            traveler = optionalTraveler.get();
            traveler.setAvatar(event.getUser().getAvatarUrl());
            traveler.setName(event.getUser().getName());
            traveler.setDiscriminator(event.getUser().getDiscriminator());
        } else {
            traveler = new Traveler(event.getUser());
        }

        return this.travelerRepository.save(traveler);
    }

    /**
     * Called when an exception occurs during the execution of an {@link ExecutableItem}.
     *
     * @param event
     *         The {@link DispatchEvent} used when the error occurred.
     * @param item
     *         The {@link ExecutableItem} generating the error.
     * @param exception
     *         The {@link Exception} thrown.
     */
    @Override
    public void handleException(DispatchEvent event, ExecutableItem item, Exception exception) {

        if (exception instanceof EmbedException ee) {
            if (event.getInteraction().isAcknowledged()) {
                event.getInteraction().getHook().editOriginalEmbeds(ee.toEmbed().build()).queue();
            } else {
                event.getInteraction().replyEmbeds(ee.toEmbed().build()).setEphemeral(true).queue();
            }

            return;
        }

        LOGGER.warn("{}: An exception occurred", event.getPath(), exception);

        Sentry.withScope(scope -> {
            scope.setTag("interaction", event.getPath().toString());
            Sentry.captureException(exception);
        });

        if (event.getInteraction().isAcknowledged()) {
            event.getInteraction().getHook().editOriginal("An error occurred.").queue();
        } else {
            event.getInteraction().reply("An error occurred.").setEphemeral(true).queue();
        }
    }

    /**
     * Called when {@link DispatchEvent#getPath()} did not match any {@link InteractionItem}.
     *
     * @param event
     *         The unmatched {@link DispatchEvent}.
     */
    @Override
    public void handleNoAction(DispatchEvent event) {

        LOGGER.warn("Well, that's strange: {}", event.getPath());
        Sentry.withScope(scope -> {
            scope.setLevel(SentryLevel.WARNING);
            scope.setTag("interaction", event.getPath().toString());
            Sentry.captureMessage("Unknown interaction path received.");
        });

        if (event.getInteraction().isAcknowledged()) {
            event.getInteraction().getHook().editOriginal("An error occurred.").queue();
        } else {
            event.getInteraction().reply("An error occurred.").setEphemeral(true).queue();
        }
    }

    /**
     * Called when an {@link InteractionItem} has been matched but could not be executed due to its filter ({@link
     * InteractionItem#canExecute(Interaction)}.
     *
     * @param event
     *         The {@link DispatchEvent} used.
     * @param item
     *         The {@link InteractionItem} that could not be executed.
     */
    @Override
    public void handleNonExecutable(DispatchEvent event, InteractionItem item) {

        LOGGER.warn("{}: Non executable interaction.", event.getPath());

        if (event.getInteraction().isAcknowledged()) {
            event.getInteraction().getHook().editOriginal("An error occurred.").queue();
        } else {
            event.getInteraction().reply("An error occurred.").setEphemeral(true).queue();
        }
    }
}
