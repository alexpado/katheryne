package me.alexpado.katheryne;

import fr.alexpado.jda.DiscordBotImpl;
import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.interfaces.ICommandHandler;
import fr.alexpado.jda.services.commands.interfaces.ICommandHelper;
import me.alexpado.katheryne.discord.CommandHelper;
import me.alexpado.katheryne.discord.listeners.DatabaseUpdater;
import me.alexpado.katheryne.discord.listeners.JdaStore;
import me.alexpado.katheryne.genshin.repositories.AdventurerGuildRepository;
import me.alexpado.katheryne.genshin.repositories.TravelerRepository;
import me.alexpado.katheryne.slash.KatheryneSlash;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@EnableScheduling
public class GenshinCoopApplication extends DiscordBotImpl {

    public static final String INV_LINK = "https://discord.com/oauth2/authorize?client_id=%s&permissions=%s&scope=bot%%20applications.commands";

    public static boolean GLOBAL_UID_LOCK = false;

    private final ApplicationContext        context;
    private final TravelerRepository        travelerRepository;
    private final AdventurerGuildRepository adventurerGuildRepository;
    private final CommandHelper             commandHelper;
    private final JdaStore                  store;
    private final KatheryneSlash            slash;

    GenshinCoopApplication(@Value("${discord.token}") String token, ApplicationContext context, TravelerRepository travelerRepository, AdventurerGuildRepository adventurerGuildRepository, CommandHelper commandHelper, JdaStore store, KatheryneSlash slash) {

        super();
        this.context                   = context;
        this.travelerRepository        = travelerRepository;
        this.adventurerGuildRepository = adventurerGuildRepository;
        this.commandHelper             = commandHelper;
        this.store                     = store;
        this.slash                     = slash;

        String notice = """
                *Commands through message will soon be removed !*
                ***Please*** use the command `slash` to learn more about this (user & **admin**)
                """;

        this.getCommandHandler().setNotice(notice);
        this.login(token);
    }

    public static void main(String[] args) {

        SpringApplication.run(GenshinCoopApplication.class, args);
    }

    /**
     * Retrieve the {@link ICommandHelper} that will be used by this {@link IDiscordBot}'s {@link ICommandHandler}.
     *
     * @return An {@link ICommandHelper} implementation.
     */
    @Override
    public @NotNull ICommandHelper getCommandHelper() {
        return this.commandHelper;
    }

    /**
     * Retrieve a list of {@link ListenerAdapter} to register before the {@link IDiscordBot} login.
     *
     * @return A list of {@link ListenerAdapter}.
     */
    @Override
    public @NotNull List<ListenerAdapter> getListenerAdapters() {
        return Arrays.asList(this.store, new DatabaseUpdater(this.travelerRepository, this.adventurerGuildRepository), this.slash);
    }

    @Override
    public void onReady(@NotNull ReadyEvent event) {
        this.slash.prepare(event, this.context);
    }
}
