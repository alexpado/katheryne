package me.alexpado.katheryne.commissions.enums;

public enum CommissionType {

    BOSS("Boss", true),
    BOUNTY("Bounties", true),
    DOMAIN("Domains", true),
    GRINDING("Grinding", false),
    MOB("Mob Farming", false),
    RESOURCE("Resources Farming", false),
    CHALLENGE("Challenge", true),
    EVENT("Event", true),
    WEI("Wei", true),
    TEAPOT("Serenitea pot", true),
    OTHER("Other", true);

    final String  displayName;
    final boolean useUid;

    CommissionType(String displayName, boolean prioritizeUid) {
        this.displayName = displayName;
        this.useUid      = prioritizeUid;
    }

    public String getDisplayName() {

        return displayName;
    }

    public boolean shouldUseUid() {
        return useUid;
    }
}
