package me.alexpado.katheryne.commissions.commands;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.DiscordCommand;
import fr.alexpado.jda.services.commands.annotations.Command;
import fr.alexpado.jda.services.commands.annotations.Param;
import fr.alexpado.jda.services.commands.interfaces.ICommand;
import fr.alexpado.jda.services.commands.interfaces.ICommandMeta;
import me.alexpado.katheryne.commissions.commands.meta.BoardCommandMeta;
import me.alexpado.katheryne.commissions.entities.Board;
import me.alexpado.katheryne.commissions.entities.Notification;
import me.alexpado.katheryne.commissions.repositories.BoardRepository;
import me.alexpado.katheryne.commissions.repositories.NotificationRepository;
import me.alexpado.katheryne.discord.Utils;
import me.alexpado.katheryne.discord.interfaces.IKatheryneContext;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import me.alexpado.katheryne.messages.EmbedException;
import me.alexpado.katheryne.messages.base.NotEnoughPermissionEmbed;
import me.alexpado.katheryne.messages.board.*;
import me.alexpado.katheryne.messages.guild.WrongChannelMentionEmbed;
import me.alexpado.katheryne.messages.helps.BoardHelpEmbed;
import me.alexpado.katheryne.messages.profiles.WrongServerRegionEmbed;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

@Service
public class BoardCommand extends DiscordCommand {

    private final BoardRepository        boardRepository;
    private final NotificationRepository notificationRepository;

    public BoardCommand(IDiscordBot discordBot, BoardRepository boardRepository, NotificationRepository notificationRepository) {
        super(discordBot);
        this.boardRepository        = boardRepository;
        this.notificationRepository = notificationRepository;
        discordBot.getCommandHandler().register(this);
    }

    private Optional<Board> getBoardFromChannel(IKatheryneContext context, String mention) {
        if (!Utils.hasBasicPrivileges(context)) {
            throw new EmbedException(new NotEnoughPermissionEmbed(context));
        }

        Optional<TextChannel> optionalTextChannel = Utils.getChannel(context, mention);

        if (optionalTextChannel.isEmpty()) {
            throw new EmbedException(new WrongChannelMentionEmbed(context));
        }

        return this.getBoardFromChannel(context, optionalTextChannel.get());
    }

    private Optional<Board> getBoardFromChannel(IKatheryneContext context, TextChannel channel) {
        long guildId   = context.getAdventurerGuild().getId();
        long channelId = channel.getIdLong();
        return this.boardRepository.findByGuildIdAndChannelId(guildId, channelId);
    }

    private void changeNotification(IKatheryneContext context, ServerRegion region, String mention, Consumer<Notification> consumer) {

        Board board = this.getBoardFromChannel(context, mention).orElseThrow(() -> new EmbedException(new BoardNotFoundEmbed(context)));

        if (!board.isRegionCompatible(region)) {
            throw new EmbedException(new BoardNotificationUnmodifiableEmbed(context));
        }

        List<Notification> notifications = this.notificationRepository.findAllByBoardId(board.getId());
        notifications.forEach(consumer);
        this.notificationRepository.saveAll(notifications);
    }

    @Command("[channel] create [region]")
    public EmbedBuilder createServerBoard(IKatheryneContext context, @Param("channel") String channel, @Param("region") String region) {

        if (!Utils.hasBasicPrivileges(context)) {
            return new NotEnoughPermissionEmbed(context);
        }

        ServerRegion serverRegion = ServerRegion.fromName(region, false)
                .orElseThrow(() -> new EmbedException(new WrongServerRegionEmbed(context, true)));

        Optional<TextChannel> optionalTextChannel = Utils.getChannel(context, channel);

        if (optionalTextChannel.isEmpty()) {
            return new WrongChannelMentionEmbed(context);
        }

        TextChannel textChannel = optionalTextChannel.get();

        if (!textChannel.canTalk() || !textChannel.getGuild().getSelfMember().hasPermission(textChannel, Permission.MESSAGE_EMBED_LINKS)) {
            return new NeedMorePermissionEmbed(context);
        }

        Optional<Board> optionalBoard = this.getBoardFromChannel(context, optionalTextChannel.get());

        if (optionalBoard.isPresent()) {
            return new BoardAlreadyExistsEmbed(context);
        }

        Board board = new Board(context.getAdventurerGuild(), serverRegion, optionalTextChannel.get().getIdLong());
        board.setRefreshable(true);
        board = this.boardRepository.save(board);

        if (serverRegion == ServerRegion.ALL) {
            for (ServerRegion value : ServerRegion.values()) {
                if (value != ServerRegion.ALL) {
                    this.notificationRepository.save(new Notification(board, value));
                }
            }
        } else {
            this.notificationRepository.save(new Notification(board, serverRegion));
        }

        return new BoardAddedEmbed(context);
    }

    @Command("[channel] remove")
    public EmbedBuilder removeServerBoard(IKatheryneContext context, @Param("channel") String channel) {

        Board              board         = this.getBoardFromChannel(context, channel).orElseThrow(() -> new EmbedException(new BoardNotFoundEmbed(context)));
        List<Notification> notifications = this.notificationRepository.findAllByBoardId(board.getId());

        // Removing the board message
        if (board.getMessageId() != null) {
            Guild       guild        = context.getEvent().getGuild();
            TextChannel boardChannel = Optional.ofNullable(guild.getTextChannelById(board.getChannelId())).orElseThrow(() -> new EmbedException(new BoardInaccessibleEmbed(context)));
            boardChannel.deleteMessageById(board.getMessageId()).complete();
        }

        this.notificationRepository.deleteAll(notifications);
        this.boardRepository.delete(board);
        return new BoardRemovedEmbed(context);
    }

    @Command(value = "[channel]", order = 1)
    public EmbedBuilder displayBoardDetails(IKatheryneContext context, @Param("channel") String channel) {

        Board              board         = this.getBoardFromChannel(context, channel).orElseThrow(() -> new EmbedException(new BoardNotFoundEmbed(context)));
        List<Notification> notifications = this.notificationRepository.findAllByBoardId(board.getId());
        return new BoardDetailsEmbed(context, board, notifications);
    }

    @Command("[channel] notification [region] change message...")
    public EmbedBuilder modifyNotificationMessage(IKatheryneContext context, @Param("channel") String channel, @Param("region") String region, @Param("message") String content) {

        ServerRegion serverRegion = ServerRegion.fromName(region, false)
                .orElseThrow(() -> new EmbedException(new WrongServerRegionEmbed(context, true)));

        this.changeNotification(context, serverRegion, channel, notification -> {
            if (notification.isRegionCompatible(serverRegion)) {
                notification.setContent(content);
            }
        });

        return new BoardUpdatedEmbed(context);
    }

    @Command("[channel] notification [region] trigger")
    public EmbedBuilder triggerGlobalNotification(IKatheryneContext context, @Param("channel") String channel, @Param("region") String region) {

        ServerRegion serverRegion = ServerRegion.fromName(region, false)
                .orElseThrow(() -> new EmbedException(new WrongServerRegionEmbed(context, true)));

        Board              board         = this.getBoardFromChannel(context, channel).orElseThrow(() -> new EmbedException(new BoardNotFoundEmbed(context)));
        List<Notification> notifications = this.notificationRepository.findAllByBoardId(board.getId());

        for (Notification notification : notifications) {
            if (notification.isRegionCompatible(serverRegion)) {
                notification.setEnabled(true);
            }
        }

        board.setNotifiable(true);

        this.boardRepository.save(board);
        this.notificationRepository.saveAll(notifications);

        return new BoardNotificationTriggeredEmbed(context);
    }

    @Command(value = "help", order = 0)
    public EmbedBuilder displayHelp(IKatheryneContext context) {
        return new BoardHelpEmbed(context, this.getBot());
    }

    /**
     * Retrieve the command meta for this {@link ICommand}.
     *
     * @return An {@link ICommandMeta} instance.
     */
    @Override
    public ICommandMeta getMeta() {
        return new BoardCommandMeta();
    }
}
