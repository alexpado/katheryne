package me.alexpado.katheryne.commissions.commands;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.DiscordCommand;
import fr.alexpado.jda.services.commands.annotations.Command;
import fr.alexpado.jda.services.commands.annotations.Param;
import fr.alexpado.jda.services.commands.interfaces.ICommand;
import fr.alexpado.jda.services.commands.interfaces.ICommandMeta;
import me.alexpado.katheryne.commissions.enums.CommissionType;
import me.alexpado.katheryne.discord.interfaces.IKatheryneContext;
import net.dv8tion.jda.api.EmbedBuilder;

public class AliasedCommissionCommand extends DiscordCommand {

    private final CommissionType    type;
    private final CommissionCommand command;

    public AliasedCommissionCommand(IDiscordBot discordBot, CommissionType type, CommissionCommand command) {
        super(discordBot);
        this.type    = type;
        this.command = command;
    }

    /**
     * Retrieve the command meta for this {@link ICommand}.
     *
     * @return An {@link ICommandMeta} instance.
     */
    @Override
    public ICommandMeta getMeta() {
        return new ICommandMeta() {
            @Override
            public String getLabel() {
                return "c-" + type.name().toLowerCase();
            }

            @Override
            public String getDescription() {
                return null;
            }

            @Override
            public String getHelp() {
                return null;
            }
        };
    }

    @Command("message...")
    public EmbedBuilder exec(IKatheryneContext context, @Param("message") String message) {
        return this.command.createCommission(context, type.name().toLowerCase(), message);
    }
}
