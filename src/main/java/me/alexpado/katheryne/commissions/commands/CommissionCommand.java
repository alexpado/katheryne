package me.alexpado.katheryne.commissions.commands;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.DiscordCommand;
import fr.alexpado.jda.services.commands.annotations.Command;
import fr.alexpado.jda.services.commands.annotations.Param;
import fr.alexpado.jda.services.commands.interfaces.ICommand;
import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import fr.alexpado.jda.services.commands.interfaces.ICommandMeta;
import fr.alexpado.jda.services.tools.embed.EmbedPage;
import me.alexpado.katheryne.commissions.commands.meta.CommissionCommandMeta;
import me.alexpado.katheryne.commissions.entities.Commission;
import me.alexpado.katheryne.commissions.enums.CommissionType;
import me.alexpado.katheryne.commissions.events.CommissionUpdateEvent;
import me.alexpado.katheryne.commissions.repositories.CommissionRepository;
import me.alexpado.katheryne.discord.interfaces.IKatheryneContext;
import me.alexpado.katheryne.genshin.entities.Profile;
import me.alexpado.katheryne.genshin.entities.Restriction;
import me.alexpado.katheryne.genshin.repositories.ProfileRepository;
import me.alexpado.katheryne.genshin.repositories.RestrictionRepository;
import me.alexpado.katheryne.messages.NeatEmbedPage;
import me.alexpado.katheryne.messages.commissions.*;
import me.alexpado.katheryne.messages.helps.CommissionHelpEmbed;
import net.dv8tion.jda.api.EmbedBuilder;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Component
public class CommissionCommand extends DiscordCommand {

    private final ProfileRepository         profileRepository;
    private final CommissionRepository      commissionRepository;
    private final RestrictionRepository     restrictionRepository;
    private final ApplicationEventPublisher publisher;

    /**
     * Creates a new {@link DiscordCommand} instance and register it immediately.
     *
     * @param discordBot
     *         The {@link IDiscordBot} associated with this command.
     * @param profileRepository
     *         The {@link JpaRepository} allowing interaction with {@link Profile}.
     * @param commissionRepository
     *         The {@link JpaRepository} allowing interaction with {@link Commission}.
     * @param restrictionRepository
     *         The {@link JpaRepository} allowing interaction with {@link Restriction}.
     * @param publisher
     *         The {@link ApplicationEventPublisher} allowing to send application event.
     */
    protected CommissionCommand(IDiscordBot discordBot, ProfileRepository profileRepository, CommissionRepository commissionRepository, RestrictionRepository restrictionRepository, ApplicationEventPublisher publisher) {

        super(discordBot);
        this.profileRepository     = profileRepository;
        this.commissionRepository  = commissionRepository;
        this.restrictionRepository = restrictionRepository;
        this.publisher             = publisher;
        discordBot.getCommandHandler().register(this);

        // Create aliases.
        for (CommissionType type : CommissionType.values()) {
            discordBot.getCommandHandler().register(new AliasedCommissionCommand(discordBot, type, this));
        }
    }

    /**
     * Retrieve the command meta for this {@link ICommand}.
     *
     * @return An {@link ICommandMeta} instance.
     */
    @Override
    public ICommandMeta getMeta() {

        return new CommissionCommandMeta();
    }

    /**
     * Display the help for the current command.
     *
     * @return An {@link EmbedBuilder} containing the message to display.
     */
    @Command("help")
    public EmbedBuilder viewCommissionTypes(ICommandContext context) {

        return new CommissionHelpEmbed(context);
    }

    /**
     * Display all opened commission by Travelers.
     *
     * @param context
     *         The {@link ICommandContext} for this execution.
     *
     * @return An {@link EmbedBuilder} or {@link EmbedPage} to display.
     */
    @Command("list")
    public Object viewCommissions(ICommandContext context) {

        List<Commission> commissions = this.commissionRepository.findAllByClosedIsFalse();

        if (commissions.isEmpty()) {
            return new EmbedBuilder().setDescription("No commission found.");
        }

        String title = String.format("Opened commissions (%s)", commissions.size());
        return new NeatEmbedPage<>(this.getBot(), commissions, 10, title);
    }

    @Command("create [type] message...")
    public EmbedBuilder createCommission(IKatheryneContext context, @Param("type") String type, @Param("message") String message) {

        Optional<Restriction> optionalRestriction = context.getTraveler().getRestriction(this.restrictionRepository);
        String                applicablePrefix    = this.getBot().getCommandHelper().getApplicablePrefix(context.getEvent());

        if (optionalRestriction.isPresent()) {
            return new TravelerRestrictedEmbed(context, optionalRestriction.get());
        }

        Optional<Profile> optionalProfile = this.profileRepository.findByTravelerAndMainIsTrue(context.getTraveler());

        if (optionalProfile.isEmpty()) {
            return new FillProfileErrorEmbed(context, applicablePrefix);
        }

        if (this.commissionRepository.findByProfileTravelerIdAndClosedIsFalse(context.getTraveler().getId()).isPresent()) {
            return new CommissionAlreadyOpenedEmbed(context);
        }

        if (message.length() > 255) {
            return new CommissionMessageTooLongEmbed(context);
        }


        CommissionType commissionType;

        try {
            commissionType = CommissionType.valueOf(type.toUpperCase());
        } catch (IllegalArgumentException e) {
            return new CommissionTypeNotRecognizedEmbed(context, applicablePrefix);
        }

        Commission commission = new Commission();

        commission.setProfile(optionalProfile.get());
        commission.setType(commissionType);
        commission.setContent(message);
        commission.setClosed(false);
        commission.setOpenedAt(LocalDateTime.now());
        commission.setClosedAt(null);

        this.commissionRepository.save(commission);

        this.publisher.publishEvent(new CommissionUpdateEvent(this, commission));
        return new CommissionOpenedEmbed(context, true);
    }

    @Command("close")
    public EmbedBuilder closeCommission(IKatheryneContext context) {

        Optional<Commission> optionalCommission = this.commissionRepository.findByProfileTravelerIdAndClosedIsFalse(context.getTraveler().getId());

        if (optionalCommission.isEmpty()) {
            return new CommissionNotOpenedEmbed(context);
        }

        Commission commission = optionalCommission.get();

        commission.setClosedAt(LocalDateTime.now());
        commission.setClosed(true);

        this.commissionRepository.save(commission);
        this.publisher.publishEvent(new CommissionUpdateEvent(this, commission));
        return new CommissionClosedEmbed(context);
    }

}
