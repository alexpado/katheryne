package me.alexpado.katheryne.commissions.entities;

import me.alexpado.katheryne.genshin.enums.ServerRegion;
import net.dv8tion.jda.api.entities.TextChannel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Optional;

@Entity
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne
    private Board board;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ServerRegion region;

    @Nullable
    private String content;

    private boolean enabled = false;

    public Notification() {
    }

    public Notification(@NotNull Board board, @NotNull ServerRegion region) {
        this.board  = board;
        this.region = region;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public @NotNull Board getBoard() {
        return board;
    }

    public void setBoard(@NotNull Board board) {
        this.board = board;
    }

    public ServerRegion getRegion() {
        return region;
    }

    public void setRegion(ServerRegion region) {
        this.region = region;
    }

    public @Nullable String getContent() {
        return content;
    }

    public void setContent(@Nullable String content) {
        this.content = content;
    }

    public @NotNull String getEffectiveContent() {
        return Optional.ofNullable(this.getContent()).orElse("A new commission has been posted !");
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isRegionCompatible(ServerRegion region) {

        return this.region == ServerRegion.ALL || region == ServerRegion.ALL || region == this.region;
    }

    public void send(TextChannel channel) {
        channel.sendMessage(this.getEffectiveContent()).queue(message -> message.delete().queue());
        this.enabled = false;
    }
}
