package me.alexpado.katheryne.commissions.entities;

import me.alexpado.katheryne.GenshinCoopApplication;
import me.alexpado.katheryne.commissions.enums.CommissionType;
import me.alexpado.katheryne.discord.interfaces.Fieldable;
import me.alexpado.katheryne.genshin.entities.Profile;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
public class Commission implements Fieldable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long           id;
    @ManyToOne
    @NotNull
    private Profile        profile;
    @Enumerated(EnumType.STRING)
    @NotNull
    private CommissionType type;
    @NotNull
    private String         content;
    private boolean        closed = false;
    @NotNull
    private LocalDateTime  openedAt;
    @Nullable
    private LocalDateTime  closedAt;

    public static Commission create(Profile profile, CommissionType type, String message) {
        Commission commission = new Commission();

        commission.setProfile(profile);
        commission.setType(type);
        commission.setContent(message);
        commission.setClosed(false);
        commission.setOpenedAt(LocalDateTime.now());
        commission.setClosedAt(null);

        return commission;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public @NotNull Profile getProfile() {
        return profile;
    }

    public void setProfile(@NotNull Profile profile) {
        this.profile = profile;
    }

    public @NotNull CommissionType getType() {
        return type;
    }

    public void setType(@NotNull CommissionType type) {
        this.type = type;
    }

    public @NotNull String getContent() {
        return content;
    }

    public void setContent(@NotNull String content) {
        this.content = content;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public @NotNull LocalDateTime getOpenedAt() {
        return openedAt;
    }

    public void setOpenedAt(@NotNull LocalDateTime openedAt) {
        this.openedAt = openedAt;
    }

    public @Nullable LocalDateTime getClosedAt() {
        return closedAt;
    }

    public void setClosedAt(@Nullable LocalDateTime closedAt) {
        this.closedAt = closedAt;
    }

    @Override
    public MessageEmbed.Field toField() {

        String title       = "[%sWL %s] %s%s";
        String description = "%s • <@%s>\n%s";

        Integer worldLevel = this.getProfile().getWorldLevel();
        String  server     = this.getProfile().getRegion().name();
        String  postTime   = this.openedAt.format(DateTimeFormatter.ofPattern("hh:mm a"));

        // Doing null-checks here
        String wl  = worldLevel == null ? "Unknown" : String.valueOf(worldLevel);
        String srv = server + " • ";

        String displayName;

        if (this.getType().shouldUseUid() && !this.getProfile().getTraveler().isUidHidden() && !GenshinCoopApplication.GLOBAL_UID_LOCK) {
            displayName = this.getProfile().getPublicUid(true) + " - ";
        } else {
            displayName = "";
        }

        String fieldTitle       = String.format(title, srv, wl, displayName, this.type.getDisplayName());
        String fieldDescription = String.format(description, postTime, this.getProfile().getTraveler().getId(), this.getContent());

        return new MessageEmbed.Field(fieldTitle, fieldDescription, false);
    }

    public boolean autoClose() {

        if (Duration.between(this.openedAt, LocalDateTime.now()).getSeconds() > 60 * 20) {
            this.closed   = true;
            this.closedAt = LocalDateTime.now();
            return true;
        }

        return false;
    }
}
