package me.alexpado.katheryne.commissions.entities;

import me.alexpado.katheryne.genshin.entities.AdventurerGuild;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import net.dv8tion.jda.api.requests.ErrorResponse;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Board {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ServerRegion region;

    @ManyToOne
    @NotNull
    private AdventurerGuild guild;

    @NotNull
    private Long channelId;

    @Nullable
    private Long messageId;

    private boolean refreshable = true;

    private boolean notifiable = false;

    public Board() {
    }

    public Board(@NotNull AdventurerGuild guild, @NotNull ServerRegion region, @NotNull Long channelId) {
        this.guild     = guild;
        this.region    = region;
        this.channelId = channelId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public @NotNull ServerRegion getRegion() {
        return region;
    }

    public void setRegion(@NotNull ServerRegion region) {
        this.region = region;
    }

    public @NotNull AdventurerGuild getGuild() {
        return guild;
    }

    public void setGuild(@NotNull AdventurerGuild guild) {
        this.guild = guild;
    }

    public @NotNull Long getChannelId() {
        return channelId;
    }

    public void setChannelId(@NotNull Long channelId) {
        this.channelId = channelId;
    }

    public @Nullable Long getMessageId() {
        return messageId;
    }

    public void setMessageId(@Nullable Long messageId) {
        this.messageId = messageId;
    }

    public boolean isRefreshable() {
        return refreshable;
    }

    public void setRefreshable(boolean shouldRefresh) {
        this.refreshable = shouldRefresh;
    }

    public boolean isNotifiable() {
        return notifiable;
    }

    public void setNotifiable(boolean notify) {
        this.notifiable = notify;
    }

    public boolean isRegionCompatible(ServerRegion region) {

        return this.region == ServerRegion.ALL || region == ServerRegion.ALL || region == this.region;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        Board board = (Board) o;
        return Objects.equals(this.getId(), board.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getId());
    }

    public Message createMessage(TextChannel channel) {
        Message message = null;
        if (this.getMessageId() == null) {
            message = channel.sendMessageEmbeds(new EmbedBuilder().setDescription("Loading Board...").build()).complete();
            this.setMessageId(message.getIdLong());
        } else {
            try {
                message = channel.retrieveMessageById(this.getMessageId()).complete();
            } catch (ErrorResponseException e) {
                if (e.getErrorResponse() == ErrorResponse.UNKNOWN_MESSAGE) {
                    message = channel.sendMessageEmbeds(new EmbedBuilder().setDescription("Loading Board...").build()).complete();
                    this.setMessageId(message.getIdLong());
                }
            }
        }

        return message;
    }
}
