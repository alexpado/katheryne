package me.alexpado.katheryne.commissions.repositories;

import me.alexpado.katheryne.commissions.entities.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {

    List<Notification> findAllByBoardId(Long board_id);

}
