package me.alexpado.katheryne.commissions.repositories;

import me.alexpado.katheryne.commissions.entities.Commission;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommissionRepository extends JpaRepository<Commission, Long> {

    List<Commission> findAllByClosedIsFalse();

    Optional<Commission> findByProfileTravelerIdAndClosedIsFalse(Long travelerId);

    List<Commission> findAllByClosedIsFalseAndProfileRegion(ServerRegion region);
}
