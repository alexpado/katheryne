package me.alexpado.katheryne.commissions.repositories;


import me.alexpado.katheryne.commissions.entities.Board;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface BoardRepository extends JpaRepository<Board, Long> {

    @Query("SELECT b FROM Board b WHERE b.notifiable = true OR b.refreshable = true")
    List<Board> findAllByRunnable();

    List<Board> findAllByRegionIn(Collection<ServerRegion> region);

    Optional<Board> findByGuildIdAndChannelId(long guild_id, Long channelId);

    List<Board> findAllByGuildId(long guild_id);

}
