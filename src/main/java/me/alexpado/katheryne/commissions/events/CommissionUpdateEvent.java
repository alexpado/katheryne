package me.alexpado.katheryne.commissions.events;

import me.alexpado.katheryne.commissions.entities.Commission;
import org.springframework.context.ApplicationEvent;

public class CommissionUpdateEvent extends ApplicationEvent {

    private final Commission commission;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param source
     *         the object on which the event initially occurred or with which the event is associated (never {@code
     *         null})
     * @param commission
     *         The {@link Commission} that has been updated.
     */
    public CommissionUpdateEvent(Object source, Commission commission) {
        super(source);
        this.commission = commission;
    }

    public Commission getCommission() {
        return commission;
    }
}
