package me.alexpado.katheryne.genshin.enums;

import me.alexpado.katheryne.genshin.entities.Profile;
import me.alexpado.katheryne.genshin.entities.Traveler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public enum ServerRegion {

    NA('6', true),
    EU('7', true),
    ASIA('8', true),
    SAR('9', true),
    ALL('*', false);

    private final boolean available;
    private final char    uidPrefix;

    ServerRegion(char uidPrefix, boolean available) {
        this.uidPrefix = uidPrefix;
        this.available = available;
    }

    /**
     * Retrieve the {@link ServerRegion} from the provided name. Unlike {@link #valueOf(String)}, this method isn't
     * case-sensitive.
     *
     * @param name
     *         The server region name.
     * @param onlyAvailable
     *         Define if only available region should be checked.
     *
     * @return The {@link ServerRegion} matched, or empty if none is found.
     */
    public static Optional<ServerRegion> fromName(@Nullable String name, boolean onlyAvailable) {

        Stream<ServerRegion> stream = Arrays.stream(ServerRegion.values());
        if (onlyAvailable) {
            stream = stream.filter(ServerRegion::isAvailable);
        }

        return stream.filter(sr -> sr.name().equalsIgnoreCase(name)).findFirst();
    }

    /**
     * Retrieve the {@link ServerRegion} based on the first character of the provided UID.
     *
     * @param uid
     *         The UID to check.
     *
     * @return The {@link ServerRegion} matched, or empty if none is found.
     *
     * @throws IllegalArgumentException
     *         Thrown if the provided UID is empty.
     */
    public static Optional<ServerRegion> fromUid(@NotNull String uid) {
        if (uid.isEmpty()) {
            throw new IllegalArgumentException("Cannot handle empty UID.");
        }

        return Arrays.stream(ServerRegion.values())
                .filter(ServerRegion::isAvailable)
                .filter(sr -> sr.isUidCompatible(uid))
                .findFirst();
    }

    public static List<ServerRegion> availableRegions(ServerRegion other) {
        if (other == ALL) {
            return Arrays.asList(NA, EU, ASIA, SAR);
        }
        return Collections.singletonList(other);
    }

    /**
     * Check if this {@link ServerRegion} should be available to {@link Traveler} while configuring a {@link Profile}.
     *
     * @return {@code true} if available to {@link Traveler}, {@code false} otherwise.
     */
    public boolean isAvailable() {
        return available;
    }

    /**
     * Check if the provided UID is compatible with the current {@link ServerRegion}.
     *
     * @param uid
     *         The UID to check.
     *
     * @return True if the UID is compatible, false otherwise.
     */
    public boolean isUidCompatible(@NotNull String uid) {
        if (uid.isEmpty()) {
            throw new IllegalArgumentException("Cannot handle empty UID.");
        }

        return uid.toCharArray()[0] == this.uidPrefix;
    }

}
