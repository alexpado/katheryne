package me.alexpado.katheryne.genshin.repositories;

import me.alexpado.katheryne.genshin.entities.Profile;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Integer> {

    Optional<Profile> findByTravelerAndRegion(Traveler traveler, ServerRegion region);

    Optional<Profile> findByTravelerAndMainIsTrue(Traveler traveler);

    List<Profile> findAllByTraveler(Traveler traveler);

    List<Profile> findAllByUid(String uid);
}
