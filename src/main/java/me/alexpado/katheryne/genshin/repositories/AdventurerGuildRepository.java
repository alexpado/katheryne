package me.alexpado.katheryne.genshin.repositories;

import me.alexpado.katheryne.genshin.entities.AdventurerGuild;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdventurerGuildRepository extends JpaRepository<AdventurerGuild, Long> {

}
