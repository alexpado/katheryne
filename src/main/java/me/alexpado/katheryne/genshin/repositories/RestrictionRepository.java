package me.alexpado.katheryne.genshin.repositories;

import me.alexpado.katheryne.genshin.entities.Restriction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RestrictionRepository extends JpaRepository<Restriction, Long> {

    List<Restriction> findAllByTravelerId(Long traveler_id);

}
