package me.alexpado.katheryne.genshin.entities;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.commissions.entities.Commission;
import me.alexpado.katheryne.genshin.repositories.ProfileRepository;
import me.alexpado.katheryne.genshin.repositories.RestrictionRepository;
import me.alexpado.katheryne.messages.base.TravelerProfile;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Entity
public class Traveler {

    @Id
    private long id;

    @NotNull
    private String name;

    @NotNull
    private String discriminator;

    @Nullable
    private String avatar;

    private boolean uidHidden = false;

    private boolean admin = false;

    @Nullable
    private String note;

    public Traveler() {
    }

    public Traveler(User user) {

        this.id            = user.getIdLong();
        this.name          = user.getName();
        this.discriminator = user.getDiscriminator();
        this.avatar        = user.getAvatarUrl();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public @NotNull String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    public @NotNull String getDiscriminator() {
        return discriminator;
    }

    public void setDiscriminator(@NotNull String discriminator) {
        this.discriminator = discriminator;
    }

    public @Nullable String getAvatar() {
        return avatar;
    }

    public void setAvatar(@Nullable String avatar) {
        this.avatar = avatar;
    }

    public boolean isUidHidden() {
        return uidHidden;
    }

    public void setUidHidden(boolean uidHidden) {
        this.uidHidden = uidHidden;
    }

    public String getTag() {

        return String.format("%s#%s", this.getName(), this.getDiscriminator());
    }

    public boolean isAdmin() {

        return admin;
    }

    public @Nullable String getNote() {
        return note;
    }

    public void setNote(@Nullable String note) {
        this.note = note;
    }

    public EmbedBuilder asEmbed(ICommandContext context, ProfileRepository repository) {

        return new TravelerProfile(context, this, repository.findAllByTraveler(this));
    }

    public EmbedBuilder asEmbed(Interaction event, ProfileRepository repository) {
        return new TravelerProfile(event, this, repository.findAllByTraveler(this));
    }

    /**
     * Check if this {@link Traveler} is restricted. A restricted {@link Traveler} won't be able to post new {@link
     * Commission}.
     *
     * @param repository
     *         The {@link RestrictionRepository} from which {@link Restriction} can be retrieved.
     *
     * @return An {@link Optional} {@link Restriction}. If present, this traveler is restricted.
     */
    public Optional<Restriction> getRestriction(RestrictionRepository repository) {

        if (this.admin) {
            return Optional.empty(); // Admin bypass
        }

        List<Restriction> restrictions = repository.findAllByTravelerId(this.id);
        LocalDateTime     now          = LocalDateTime.now();

        for (Restriction restriction : restrictions) {
            if (restriction.getStartingAt().isBefore(now) && restriction.getEndingAt().isAfter(now)) {
                return Optional.of(restriction);
            }
        }

        return Optional.empty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        Traveler traveler = (Traveler) o;
        return this.getId() == traveler.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getId());
    }
}
