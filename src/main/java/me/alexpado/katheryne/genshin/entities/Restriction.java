package me.alexpado.katheryne.genshin.entities;

import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Restriction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @NotNull
    private Traveler author;

    @ManyToOne
    @NotNull
    private Traveler traveler;

    @NotNull
    private String reason;

    @NotNull
    private LocalDateTime startingAt;

    @NotNull
    private LocalDateTime endingAt;

    public Restriction() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public @NotNull Traveler getAuthor() {
        return author;
    }

    public void setAuthor(@NotNull Traveler author) {
        this.author = author;
    }

    public @NotNull Traveler getTraveler() {
        return traveler;
    }

    public void setTraveler(@NotNull Traveler traveler) {
        this.traveler = traveler;
    }

    public @NotNull String getReason() {
        return reason;
    }

    public void setReason(@NotNull String reason) {
        this.reason = reason;
    }

    public @NotNull LocalDateTime getStartingAt() {
        return startingAt;
    }

    public void setStartingAt(@NotNull LocalDateTime startingAt) {
        this.startingAt = startingAt;
    }

    public @NotNull LocalDateTime getEndingAt() {
        return endingAt;
    }

    public void setEndingAt(@NotNull LocalDateTime endingAt) {
        this.endingAt = endingAt;
    }

}
