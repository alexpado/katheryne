package me.alexpado.katheryne.genshin.entities;

import me.alexpado.katheryne.GenshinCoopApplication;
import me.alexpado.katheryne.discord.interfaces.Fieldable;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import net.dv8tion.jda.api.entities.MessageEmbed;

import javax.persistence.*;

@Entity
public class Profile implements Fieldable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private Traveler traveler;

    @Enumerated(EnumType.STRING)
    private ServerRegion region;

    private Integer worldLevel;

    private String uid;

    private boolean main;

    public Profile() {

    }

    public Profile(Traveler traveler, ServerRegion region) {

        this.traveler = traveler;
        this.region   = region;
        this.main     = false;
    }

    public Traveler getTraveler() {
        return traveler;
    }

    public void setTraveler(Traveler traveler) {
        this.traveler = traveler;
    }

    public ServerRegion getRegion() {
        return region;
    }

    public void setRegion(ServerRegion region) {
        this.region = region;
    }

    public Integer getWorldLevel() {
        return worldLevel;
    }

    public void setWorldLevel(Integer worldLevel) {
        this.worldLevel = worldLevel;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    /**
     * Retrieve the public representation of the UID for this {@link Profile}.
     *
     * @param commission
     *         If the display is for a commission.
     *
     * @return The public representation of the UID.
     */
    public String getPublicUid(boolean commission) {

        if (GenshinCoopApplication.GLOBAL_UID_LOCK) {
            return "**Security Lock**";
        }

        if (this.getTraveler().isUidHidden()) {
            return "*Hidden*";
        }

        if (this.getUid() == null) {
            return commission ? this.getTraveler().getTag() : "*Not Defined*";
        }

        return this.getUid();
    }

    /**
     * Retrieve the public representation of the world level for this {@link Profile}.
     *
     * @return The public representation of the world level.
     */
    public String getPublicWorldLevel() {

        if (this.getWorldLevel() == null) {
            return "*Not Defined*";
        }

        return this.getWorldLevel().toString();
    }

    /**
     * Check if the minimum requirement in profile is met to being able to post commissions.
     *
     * @return True if this Traveler can post commission, false instead.
     */
    public boolean isProfileMinimumFilled() {

        return this.getRegion() != null && this.getWorldLevel() != null;
    }

    @Override
    public MessageEmbed.Field toField() {
        return new MessageEmbed.Field(this.getRegion().name(), String.format("WL: %s • UID: %s", this.getPublicWorldLevel(), this.getPublicUid(false)), false);
    }
}
