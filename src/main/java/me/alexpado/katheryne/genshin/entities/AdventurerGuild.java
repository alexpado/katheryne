package me.alexpado.katheryne.genshin.entities;

import net.dv8tion.jda.api.entities.Guild;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "guild")
public class AdventurerGuild {

    @Id
    private long id;

    @NotNull
    private String name;

    @Nullable
    private String icon;

    @Nullable
    private String prefix;

    private boolean vip = false;

    public AdventurerGuild() {
    }

    public AdventurerGuild(Guild guild) {
        this.id     = guild.getIdLong();
        this.name   = guild.getName();
        this.icon   = guild.getIconUrl();
        this.prefix = null;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public @NotNull String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    public @Nullable String getIcon() {
        return icon;
    }

    public void setIcon(@Nullable String icon) {
        this.icon = icon;
    }

    public @Nullable String getPrefix() {
        return prefix;
    }

    public void setPrefix(@Nullable String prefix) {
        this.prefix = prefix;
    }

    public boolean isVip() {
        return vip;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        AdventurerGuild adventurerGuild = (AdventurerGuild) o;
        return this.getId() == adventurerGuild.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getId());
    }
}
