package me.alexpado.katheryne.genshin.commands;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.DiscordCommand;
import fr.alexpado.jda.services.commands.annotations.Command;
import fr.alexpado.jda.services.commands.annotations.Param;
import fr.alexpado.jda.services.commands.interfaces.ICommand;
import fr.alexpado.jda.services.commands.interfaces.ICommandMeta;
import me.alexpado.katheryne.discord.Utils;
import me.alexpado.katheryne.discord.interfaces.IKatheryneContext;
import me.alexpado.katheryne.discord.tools.TimeConverter;
import me.alexpado.katheryne.genshin.commands.meta.RestrictionCommandMeta;
import me.alexpado.katheryne.genshin.entities.Restriction;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.genshin.repositories.RestrictionRepository;
import me.alexpado.katheryne.messages.base.NotEnoughPermissionEmbed;
import me.alexpado.katheryne.messages.profiles.ProfileNotFoundEmbed;
import me.alexpado.katheryne.messages.restrict.AdminRestrictErrorEmbed;
import me.alexpado.katheryne.messages.restrict.RestrictionAppliedEmbed;
import me.alexpado.katheryne.messages.restrict.SelfRestrictErrorEmbed;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class RestrictionCommand extends DiscordCommand {

    private final RestrictionRepository restrictionRepository;

    public RestrictionCommand(IDiscordBot discordBot, RestrictionRepository restrictionRepository) {

        super(discordBot);
        this.restrictionRepository = restrictionRepository;
        discordBot.getCommandHandler().register(this);
    }

    /**
     * Retrieve the command meta for this {@link ICommand}.
     *
     * @return An {@link ICommandMeta} instance.
     */
    @Override
    public ICommandMeta getMeta() {

        return new RestrictionCommandMeta();
    }

    @Command("command...")
    public Object restrictBadTraveler(IKatheryneContext context, @Param("command") String command) {

        if (!context.getTraveler().isAdmin()) {
            return new NotEnoughPermissionEmbed(context);
        }

        Pattern pattern = Pattern.compile("((?<duration>\\S*) )?(?<tag>.*#[0-9]{4}) (?<reason>.*)");
        Matcher matcher = pattern.matcher(command);

        if (matcher.matches()) {

            String duration = matcher.group("duration");
            String tag      = matcher.group("tag");
            String reason   = matcher.group("reason");

            Optional<Traveler> optionalUser = Utils.retrieveTraveler(context.getTravelerRepository(), tag);

            if (optionalUser.isEmpty()) {
                return new ProfileNotFoundEmbed(context);
            }

            if (optionalUser.get().getId() == context.getTraveler().getId()) {
                return new SelfRestrictErrorEmbed(context);
            }

            if (optionalUser.get().isAdmin()) {
                return new AdminRestrictErrorEmbed(context);
            }

            Restriction restriction = new Restriction();

            restriction.setAuthor(context.getTraveler());
            restriction.setTraveler(optionalUser.get());
            restriction.setStartingAt(LocalDateTime.now());

            if (duration.equals("def")) {
                restriction.setEndingAt(LocalDateTime.MAX);
            } else {
                restriction.setEndingAt(LocalDateTime.now().plusSeconds(TimeConverter.fromString(duration)));
            }

            restriction.setReason(reason);

            this.restrictionRepository.save(restriction);

            return new RestrictionAppliedEmbed(context);
        }

        return null;
    }
}
