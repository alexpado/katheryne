package me.alexpado.katheryne.genshin.commands;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.DiscordCommand;
import fr.alexpado.jda.services.commands.annotations.Command;
import fr.alexpado.jda.services.commands.annotations.Param;
import fr.alexpado.jda.services.commands.interfaces.ICommand;
import fr.alexpado.jda.services.commands.interfaces.ICommandMeta;
import me.alexpado.katheryne.discord.interfaces.IKatheryneContext;
import me.alexpado.katheryne.genshin.commands.meta.TravelerNoteCommandMeta;
import me.alexpado.katheryne.genshin.entities.Profile;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.genshin.repositories.TravelerRepository;
import me.alexpado.katheryne.messages.traveler.TravelerProfileNoteDefinedEmbed;
import net.dv8tion.jda.api.EmbedBuilder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public class TravelerNoteCommand extends DiscordCommand {

    private final TravelerRepository repository;

    /**
     * Creates a new {@link DiscordCommand} instance and register it immediately.
     *
     * @param discordBot
     *         The {@link IDiscordBot} associated with this command.
     * @param repository The {@link JpaRepository} allowing interaction with {@link Traveler}.
     */
    protected TravelerNoteCommand(IDiscordBot discordBot, TravelerRepository repository) {
        super(discordBot);
        this.repository = repository;
        discordBot.getCommandHandler().register(this);
    }

    /**
     * Retrieve the command meta for this {@link ICommand}.
     *
     * @return An {@link ICommandMeta} instance.
     */
    @Override
    public ICommandMeta getMeta() {
        return new TravelerNoteCommandMeta();
    }

    @Command("note set note...")
    public EmbedBuilder setTravelerNote(IKatheryneContext context, @Param("note") String note) {
        context.getTraveler().setNote(note);
        this.repository.save(context.getTraveler());
        return new TravelerProfileNoteDefinedEmbed(context);
    }

    @Command("note remove")
    public EmbedBuilder removeTravelerNote(IKatheryneContext context) {
        context.getTraveler().setNote(null);
        this.repository.save(context.getTraveler());
        return new TravelerProfileNoteDefinedEmbed(context);
    }


}
