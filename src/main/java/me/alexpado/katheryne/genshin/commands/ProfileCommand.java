package me.alexpado.katheryne.genshin.commands;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.DiscordCommand;
import fr.alexpado.jda.services.commands.annotations.Command;
import fr.alexpado.jda.services.commands.annotations.Param;
import fr.alexpado.jda.services.commands.interfaces.ICommand;
import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import fr.alexpado.jda.services.commands.interfaces.ICommandMeta;
import me.alexpado.katheryne.GenshinCoopApplication;
import me.alexpado.katheryne.discord.DiscordTag;
import me.alexpado.katheryne.discord.Utils;
import me.alexpado.katheryne.discord.interfaces.IKatheryneContext;
import me.alexpado.katheryne.genshin.commands.meta.ProfileCommandMeta;
import me.alexpado.katheryne.genshin.entities.Profile;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import me.alexpado.katheryne.genshin.repositories.ProfileRepository;
import me.alexpado.katheryne.messages.EmbedException;
import me.alexpado.katheryne.messages.NeatEmbedPage;
import me.alexpado.katheryne.messages.base.DeprecatedCommandEmbed;
import me.alexpado.katheryne.messages.helps.ProfileHelpEmbed;
import me.alexpado.katheryne.messages.profiles.*;
import net.dv8tion.jda.api.EmbedBuilder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.util.List;
import java.util.Optional;

@Component
public class ProfileCommand extends DiscordCommand {

    private final ProfileRepository repository;

    /**
     * Creates a new {@link DiscordCommand} instance and register it immediately.
     *
     * @param discordBot
     *         The {@link IDiscordBot} associated with this command.
     * @param repository
     *         The {@link JpaRepository} allowing interaction with {@link Profile}.
     */
    protected ProfileCommand(IDiscordBot discordBot, ProfileRepository repository) {

        super(discordBot);
        this.repository = repository;
        discordBot.getCommandHandler().register(this);
    }

    /**
     * Retrieve the command meta for this {@link ICommand}.
     *
     * @return An {@link ICommandMeta} instance.
     */
    @Override
    public ICommandMeta getMeta() {

        return new ProfileCommandMeta();
    }

    @Command("help")
    public Object help(ICommandContext context) {

        return new ProfileHelpEmbed(context, this.getBot());
    }

    @Command("view")
    public Object view(IKatheryneContext context) {

        return context.getTraveler().asEmbed(context, this.repository);
    }

    @Command(value = "[region] set wl /value:[1-8]{1}/")
    public EmbedBuilder updateWorldLevel(IKatheryneContext context, @Param("region") String region, @Param("value") String value) {

        ServerRegion serverRegion = ServerRegion.fromName(region, true)
                .orElseThrow(() -> new EmbedException(new WrongServerRegionEmbed(context, false)));

        // We know that this is a number, no need to check for NumberFormatException.
        int wl = Integer.parseInt(value);

        if (wl < 0 || wl > 8) {
            return new WrongWorldLevelEmbed(context);
        }

        Profile profile = this.repository.findByTravelerAndRegion(context.getTraveler(), serverRegion)
                .orElse(new Profile(context.getTraveler(), serverRegion));

        List<Profile> profiles = this.repository.findAllByTraveler(context.getTraveler());

        if (profiles.isEmpty()) {
            profile.setMain(true);
        }

        profile.setWorldLevel(wl);
        this.repository.save(profile);
        return new WorldLevelUpdatedEmbed(context);
    }

    @Command(value = "[region] set uid /value:[6789][0-9]+/")
    public EmbedBuilder updateUid(IKatheryneContext context, @Param("region") String region, @Param("value") String value) {

        ServerRegion serverRegion = ServerRegion.fromName(region, true)
                .orElseThrow(() -> new EmbedException(new WrongServerRegionEmbed(context, false)));

        if (!serverRegion.isUidCompatible(value)) {
            return new WrongUIDEmbed(context);
        }

        Profile profile = this.repository.findByTravelerAndRegion(context.getTraveler(), serverRegion)
                .orElse(new Profile(context.getTraveler(), serverRegion));

        List<Profile> profiles = this.repository.findAllByTraveler(context.getTraveler());

        if (profiles.isEmpty()) {
            profile.setMain(true);
        }

        profile.setUid(value);
        this.repository.save(profile);
        return new UIDUpdatedEmbed(context);
    }

    @Command(value = "[region] set main")
    public EmbedBuilder setProfileAsMain(IKatheryneContext context, @Param("region") String region) {

        ServerRegion serverRegion = ServerRegion.fromName(region, true)
                .orElseThrow(() -> new EmbedException(new WrongServerRegionEmbed(context, false)));

        List<Profile> profiles = this.repository.findAllByTraveler(context.getTraveler());

        if (profiles.stream().noneMatch(profile -> profile.getRegion().equals(serverRegion))) {
            return new NoMatchedProfileEmbed(context);
        }

        for (Profile profile : profiles) {
            profile.setMain(profile.getRegion().equals(serverRegion));
        }

        this.repository.saveAll(profiles);
        return new MainProfileSetEmbed(context, serverRegion);
    }

    @Command("make uid public")
    public Object setPublicUid(IKatheryneContext context) {

        if (GenshinCoopApplication.GLOBAL_UID_LOCK) {
            return new EmbedBuilder().setDescription("Sorry Traveler, this feature is locked right now.").setColor(Color.RED);
        }

        context.getTraveler().setUidHidden(false);
        return new UIDVisibilityEmbed(context, context.getTraveler());
    }

    @Command("make uid private")
    public Object setPrivateUid(IKatheryneContext context) {

        context.getTraveler().setUidHidden(true);
        return new UIDVisibilityEmbed(context, context.getTraveler());
    }

    @Command(value = "view /mention:<@!?([0-9]+)>/", order = 0)
    public EmbedBuilder viewProfileByMention(IKatheryneContext context, @Param("mention") String mention) {
        Long               travelerId       = Long.valueOf(mention);
        Optional<Traveler> optionalTraveler = context.getTravelerRepository().findById(travelerId);
        Traveler           traveler         = optionalTraveler.orElseThrow(() -> new EmbedException(new ProfileNotFoundEmbed(context)));
        return traveler.asEmbed(context, this.repository);
    }

    @Command(value = "view /uid:[0-9]+/", order = 0)
    public Object viewProfileByUid(IKatheryneContext context, @Param("uid") String uid) {
        List<Profile> profiles = this.repository.findAllByUid(uid);

        if (profiles.isEmpty()) {
            return new ProfileNotFoundEmbed(context);
        } else if (profiles.size() == 1) {
            return profiles.get(0).getTraveler().asEmbed(context, this.repository);
        } else {
            return new NeatEmbedPage<>(this.getBot(), profiles, 10, "UID Search Result");
        }
    }

    @Command(value = "view tag...", order = 1)
    public Object viewProfileByTag(IKatheryneContext context, @Param("tag") String tag) {
        Optional<DiscordTag> optionalTag = Utils.getTag(tag);

        if (optionalTag.isPresent()) {
            Optional<Traveler> optionalUser = optionalTag.get().find(context.getTravelerRepository());
            if (optionalUser.isPresent()) {
                return optionalUser.get().asEmbed(context, this.repository);
            }
        }
        return new ProfileNotFoundEmbed(context);
    }

    @Command("set wl [value]")
    @Deprecated
    public Object setWorld(IKatheryneContext context, @Param("value") String value) {

        return new DeprecatedCommandEmbed(context);
    }

    @Command("set uid [value]")
    @Deprecated
    public EmbedBuilder setUid(IKatheryneContext context, @Param("value") String value) {

        return new DeprecatedCommandEmbed(context);
    }

    @Command("set server [servers]")
    @Deprecated
    public EmbedBuilder setServer(IKatheryneContext context, @Param("servers") String value) {

        return this.setServerRegion(context);
    }

    @Command("set region [servers]")
    @Deprecated
    public EmbedBuilder setRegion(IKatheryneContext context, @Param("servers") String value) {

        return this.setServerRegion(context);
    }

    @Deprecated
    private EmbedBuilder setServerRegion(IKatheryneContext context) {

        return new DeprecatedCommandEmbed(context);
    }

}
