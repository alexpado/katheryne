package me.alexpado.katheryne.statistics.controllers;

import me.alexpado.katheryne.statistics.entities.proxy.DailyActivity;
import me.alexpado.katheryne.statistics.entities.proxy.DailyTraveler;
import me.alexpado.katheryne.statistics.repositories.proxy.DailyActivityProxy;
import me.alexpado.katheryne.statistics.repositories.proxy.DailyTravelerProxy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/stats")
public class StatisticController {

    private final DailyActivityProxy dailyActivityProxy;
    private final DailyTravelerProxy dailyTravelerProxy;

    public StatisticController(DailyActivityProxy dailyActivityProxy, DailyTravelerProxy dailyTravelerProxy) {
        this.dailyActivityProxy = dailyActivityProxy;
        this.dailyTravelerProxy = dailyTravelerProxy;
    }

    @GetMapping(value = "/activity/{year:[0-9]{4}}/{month:[0-9]{2}}/{day:[0-9]{2}}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DailyActivity> getActivityOn(@PathVariable int year, @PathVariable int month, @PathVariable int day) {
        return this.dailyActivityProxy.getAllOn(year, month, day);
    }

    @GetMapping(value = "/activity/{year:[0-9]{4}}/{month:[0-9]{2}}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DailyActivity> getActivityOn(@PathVariable int year, @PathVariable int month, @RequestParam(value = "mode", required = false) String displayMode) {

        if (displayMode == null || displayMode.equals("all")) {
            return this.dailyActivityProxy.getAllOn(year, month);
        } else if (displayMode.equals("sum")) {
            return this.dailyActivityProxy.getSum(year, month);
        }

        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unkown display mode.");
    }

    @GetMapping(value = "/activity/{year:[0-9]{4}}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DailyActivity> getActivityOn(@PathVariable int year, @RequestParam(value = "mode", required = false) String displayMode) {

        if (displayMode == null || displayMode.equals("all")) {
            return this.dailyActivityProxy.getAllOn(year);
        } else if (displayMode.equals("sum")) {
            return this.dailyActivityProxy.getSum(year);
        } else if (displayMode.equals("monthly")) {
            return this.dailyActivityProxy.getMonthlySum(year);
        }

        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unkown display mode.");
    }

    @GetMapping(value = "/activity", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DailyActivity> getActivityOn(@RequestParam(value = "mode", required = false) String displayMode) {

        if (displayMode == null || displayMode.equals("all")) {
            return this.dailyActivityProxy.getAllOn();
        } else if (displayMode.equals("sum")) {
            return this.dailyActivityProxy.getSum();
        }

        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unkown display mode.");
    }

    // --

    @GetMapping(value = "/traveler/{year:[0-9]{4}}/{month:[0-9]{2}}/{day:[0-9]{2}}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DailyTraveler> getTravelerOn(@PathVariable int year, @PathVariable int month, @PathVariable int day, @RequestParam(value = "limit", required = false, defaultValue = "0") int limit) {
        return this.dailyTravelerProxy.getAllOn(limit, year, month, day);
    }

    @GetMapping(value = "/traveler/{year:[0-9]{4}}/{month:[0-9]{2}}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DailyTraveler> getTravelerOn(@PathVariable int year, @PathVariable int month, @RequestParam(value = "limit", required = false, defaultValue = "0") int limit) {
        return this.dailyTravelerProxy.getSum(limit, year, month);
    }

    @GetMapping(value = "/traveler/{year:[0-9]{4}}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DailyTraveler> getTravelerOn(@PathVariable int year, @RequestParam(value = "limit", required = false, defaultValue = "0") int limit) {
        return this.dailyTravelerProxy.getSum(limit, year);
    }

    @GetMapping(value = "/traveler", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DailyTraveler> getTravelerOn(@RequestParam(value = "limit", required = false, defaultValue = "0") int limit) {
        return this.dailyTravelerProxy.getSum(limit);
    }


}
