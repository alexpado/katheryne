package me.alexpado.katheryne.statistics.repositories;

import fr.alexpado.jda.services.commands.annotations.Param;
import me.alexpado.katheryne.statistics.entities.StatDailyTraveler;
import me.alexpado.katheryne.statistics.entities.interfaces.IStatDailyTraveler;
import me.alexpado.katheryne.statistics.entities.keys.StatDailyTravelerKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatDailyTravelerRepository extends JpaRepository<StatDailyTraveler, StatDailyTravelerKey> {

    @Query("SELECT sdt FROM StatDailyTraveler sdt WHERE sdt.dateYear = :year AND sdt.dateMonth = :month AND sdt.dateDay = :day")
    List<IStatDailyTraveler> getAllOn(@Param("year") int year, @Param("month") int month, @Param("day") int day);

    @Query("SELECT sdt.dateYear as dateYear, sdt.dateMonth as dateMonth, sdt.dateDay as dateDay, sdt.traveler as traveler, sdt.tag as tag, SUM(sdt.commissionAmount) as commissionAmount, SUM(sdt.autoClosed) as autoClosed FROM StatDailyTraveler sdt WHERE sdt.dateYear = :year AND sdt.dateMonth = :month GROUP BY sdt.dateYear, sdt.dateMonth, sdt.traveler ORDER BY commissionAmount DESC, autoClosed")
    List<IStatDailyTraveler> getSum(@Param("year") int year, @Param("month") int month);

    @Query("SELECT sdt.dateYear as dateYear, sdt.dateMonth as dateMonth, sdt.dateDay as dateDay, sdt.traveler as traveler, sdt.tag as tag, SUM(sdt.commissionAmount) as commissionAmount, SUM(sdt.autoClosed) as autoClosed FROM StatDailyTraveler sdt WHERE sdt.dateYear = :year GROUP BY sdt.dateYear, sdt.traveler ORDER BY commissionAmount DESC, autoClosed")
    List<IStatDailyTraveler> getSum(@Param("year") int year);

    @Query("SELECT sdt.dateYear as dateYear, sdt.dateMonth as dateMonth, sdt.dateDay as dateDay, sdt.traveler as traveler, sdt.tag as tag, SUM(sdt.commissionAmount) as commissionAmount, SUM(sdt.autoClosed) as autoClosed FROM StatDailyTraveler sdt GROUP BY sdt.traveler ORDER BY commissionAmount DESC, autoClosed")
    List<IStatDailyTraveler> getSum();

}
