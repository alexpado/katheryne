package me.alexpado.katheryne.statistics.repositories;

import fr.alexpado.jda.services.commands.annotations.Param;
import me.alexpado.katheryne.statistics.entities.StatDailyActivity;
import me.alexpado.katheryne.statistics.entities.interfaces.IStatDailyActivity;
import me.alexpado.katheryne.statistics.entities.keys.StatDailyActivityKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatDailyActivityRepository extends JpaRepository<StatDailyActivity, StatDailyActivityKey> {

    @Query("SELECT sda FROM StatDailyActivity sda WHERE sda.dateYear = :year AND sda.dateMonth = :month AND sda.dateDay = :day")
    List<IStatDailyActivity> getAllOn(@Param("year") int year, @Param("month") int month, @Param("day") int day);

    @Query("SELECT sda FROM StatDailyActivity sda WHERE sda.dateYear = :year AND sda.dateMonth = :month")
    List<IStatDailyActivity> getAllOn(@Param("year") int year, @Param("month") int month);

    @Query("SELECT sda FROM StatDailyActivity sda WHERE sda.dateYear = :year")
    List<IStatDailyActivity> getAllOn(@Param("year") int year);

    @Query("SELECT sda FROM StatDailyActivity sda")
    List<IStatDailyActivity> getAllOn();

    @Query("SELECT sda.dateYear as dateYear, sda.dateMonth as dateMonth, sda.dateDay as dateDay, SUM(sda.commissionAmount) as commissionAmount, SUM(sda.autoClosed) as autoClosed FROM StatDailyActivity sda WHERE sda.dateYear = :year AND sda.dateMonth = :month GROUP BY sda.dateYear, sda.dateMonth")
    List<IStatDailyActivity> getSum(@Param("year") int year, @Param("month") int month);

    @Query("SELECT sda.dateYear as dateYear, sda.dateMonth as dateMonth, sda.dateDay as dateDay, SUM(sda.commissionAmount) as commissionAmount, SUM(sda.autoClosed) as autoClosed FROM StatDailyActivity sda WHERE sda.dateYear = :year GROUP BY sda.dateYear")
    List<IStatDailyActivity> getSum(@Param("year") int year);

    @Query("SELECT sda.dateYear as dateYear, sda.dateMonth as dateMonth, sda.dateDay as dateDay, SUM(sda.commissionAmount) as commissionAmount, SUM(sda.autoClosed) as autoClosed FROM StatDailyActivity sda WHERE sda.dateYear = :year GROUP BY sda.dateYear, sda.dateMonth")
    List<IStatDailyActivity> getMonthlySum(@Param("year") int year);

    @Query("SELECT sda.dateYear as dateYear, sda.dateMonth as dateMonth, sda.dateDay as dateDay, SUM(sda.commissionAmount) as commissionAmount, SUM(sda.autoClosed) as autoClosed FROM StatDailyActivity sda")
    List<IStatDailyActivity> getSum();

}
