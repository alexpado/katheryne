package me.alexpado.katheryne.statistics.repositories.proxy;

import me.alexpado.katheryne.statistics.entities.interfaces.IStatDailyActivity;
import me.alexpado.katheryne.statistics.entities.proxy.DailyActivity;
import me.alexpado.katheryne.statistics.repositories.StatDailyActivityRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Class handling {@link IStatDailyActivity} proxies conversion from {@link StatDailyActivityRepository} into mutable
 * {@link DailyActivity} instance.
 *
 * @author alexpado
 * @see StatDailyActivityRepository
 * @see DailyActivity
 */
@Component
public class DailyActivityProxy {

    private final StatDailyActivityRepository repository;

    public DailyActivityProxy(StatDailyActivityRepository repository) {
        this.repository = repository;
    }

    public List<DailyActivity> getAllOn(int year, int month, int day) {
        return this.repository.getAllOn(year, month, day).stream().map(DailyActivity::new).collect(Collectors.toList());
    }

    public List<DailyActivity> getAllOn(int year, int month) {
        return this.repository.getAllOn(year, month).stream().map(DailyActivity::new).collect(Collectors.toList());
    }

    public List<DailyActivity> getAllOn(int year) {
        return this.repository.getAllOn(year).stream().map(DailyActivity::new).collect(Collectors.toList());
    }

    public List<DailyActivity> getAllOn() {
        return this.repository.getAllOn().stream().map(DailyActivity::new).collect(Collectors.toList());
    }

    public List<DailyActivity> getSum(int year, int month) {
        return this.repository.getSum(year, month).stream().map(DailyActivity::new).map(DailyActivity::flatMonth).collect(Collectors.toList());

    }

    public List<DailyActivity> getSum(int year) {
        return this.repository.getSum(year).stream().map(DailyActivity::new).map(DailyActivity::flatYear).collect(Collectors.toList());
    }

    public List<DailyActivity> getMonthlySum(int year) {
        return this.repository.getMonthlySum(year).stream().map(DailyActivity::new).map(DailyActivity::flatMonth).collect(Collectors.toList());
    }

    public List<DailyActivity> getSum() {
        return this.repository.getSum().stream().map(DailyActivity::new).collect(Collectors.toList());

    }
}
