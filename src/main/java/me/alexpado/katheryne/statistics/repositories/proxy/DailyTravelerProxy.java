package me.alexpado.katheryne.statistics.repositories.proxy;

import me.alexpado.katheryne.statistics.entities.interfaces.IStatDailyTraveler;
import me.alexpado.katheryne.statistics.entities.proxy.DailyTraveler;
import me.alexpado.katheryne.statistics.repositories.StatDailyTravelerRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class handling {@link IStatDailyTraveler} proxies conversion from {@link StatDailyTravelerRepository} into mutable
 * {@link DailyTraveler} instance.
 *
 * @author alexpado
 * @see StatDailyTravelerRepository
 * @see DailyTraveler
 */
@Component
public class DailyTravelerProxy {

    private final StatDailyTravelerRepository repository;

    public DailyTravelerProxy(StatDailyTravelerRepository repository) {
        this.repository = repository;
    }

    public List<DailyTraveler> getAllOn(int limit, int year, int month, int day) {
        Stream<DailyTraveler> stream = this.repository.getAllOn(year, month, day).stream().map(DailyTraveler::new);

        if (limit > 0) {
            stream = stream.limit(limit);
        }

        return stream.collect(Collectors.toList());
    }

    public List<DailyTraveler> getSum(int limit, int year, int month) {
        Stream<DailyTraveler> stream = this.repository.getSum(year, month).stream().map(DailyTraveler::new).map(DailyTraveler::flatMonth);

        if (limit > 0) {
            stream = stream.limit(limit);
        }

        return stream.collect(Collectors.toList());
    }

    public List<DailyTraveler> getSum(int limit, int year) {
        Stream<DailyTraveler> stream = this.repository.getSum(year).stream().map(DailyTraveler::new).map(DailyTraveler::flatYear);

        if (limit > 0) {
            stream = stream.limit(limit);
        }

        return stream.collect(Collectors.toList());
    }

    public List<DailyTraveler> getSum(int limit) {

        Stream<DailyTraveler> stream = this.repository.getSum().stream().map(DailyTraveler::new);

        if (limit > 0) {
            stream = stream.limit(limit);
        }

        return stream.collect(Collectors.toList());
    }
}
