package me.alexpado.katheryne.statistics.entities.interfaces;

import me.alexpado.katheryne.commissions.entities.Commission;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.statistics.entities.StatDailyTraveler;
import me.alexpado.katheryne.statistics.entities.proxy.DailyTraveler;
import me.alexpado.katheryne.statistics.repositories.StatDailyTravelerRepository;

import java.time.LocalDate;

/**
 * Interface used as proxy by {@link StatDailyTravelerRepository}. This is because {@link StatDailyTraveler} is based on
 * a view and is not compatible when using SUM and COUNT.
 *
 * A {@link LocalDate} can be built using {@link LocalDate#of(int, int, int)} with {@link #getDateYear()}, {@link
 * #getDateMonth()} and {@link #getDateDay()}.
 *
 * @author alexpado
 * @see DailyTraveler
 */
public interface IStatDailyTraveler {

    /**
     * Retrieves the year associated with this {@link IStatDailyTraveler}.
     *
     * @return The year
     */
    int getDateYear();

    /**
     * Retrieves the month associated with this {@link IStatDailyTraveler}.
     *
     * @return The month
     */
    int getDateMonth();

    /**
     * Retrieves the day associated with this {@link IStatDailyTraveler}.
     *
     * @return The day
     */
    int getDateDay();

    /**
     * Retrieves the {@link Traveler}'s ID associated with this {@link IStatDailyTraveler}.
     *
     * @return The {@link Traveler}'s ID.
     */
    long getTraveler();

    /**
     * Retrieves the {@link Traveler#getTag()} associated with this {@link IStatDailyTraveler}.
     *
     * @return The {@link Traveler#getTag()}.
     */
    String getTag();

    /**
     * Retrieves the {@link Commission} amount associated with this {@link IStatDailyTraveler}.
     *
     * @return The commission amount
     */
    long getCommissionAmount();

    /**
     * Retrieves the auto closed {@link Commission} amount associated with this {@link IStatDailyTraveler}.
     *
     * @return The auto closed commission amount
     */
    long getAutoClosed();

}
