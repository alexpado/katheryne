package me.alexpado.katheryne.statistics.entities.keys;


import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.statistics.entities.StatDailyTraveler;

import javax.persistence.IdClass;
import java.io.Serializable;
import java.util.Objects;

/**
 * Class describing a {@link StatDailyTraveler} ID. This class is meant to be used within an {@link IdClass} annotation,
 * even if there is no actual usage of {@link StatDailyTraveler}.
 *
 * @author alexpado
 * @see StatDailyTraveler
 */
public class StatDailyTravelerKey implements Serializable {

    private int  dateYear;
    private int  dateMonth;
    private int  dateDay;
    private long traveler;

    /**
     * Retrieves the year associated with this {@link StatDailyActivityKey}.
     *
     * @return The year
     */
    public int getDateYear() {
        return this.dateYear;
    }

    /**
     * Retrieves the month associated with this {@link StatDailyActivityKey}.
     *
     * @return The month
     */
    public int getDateMonth() {
        return this.dateMonth;
    }

    /**
     * Retrieves the day associated with this {@link StatDailyActivityKey}.
     *
     * @return The day
     */
    public int getDateDay() {
        return this.dateDay;
    }

    /**
     * Retrieves the {@link Traveler}'s ID associated with this {@link StatDailyActivityKey}.
     *
     * @return The {@link Traveler}'s ID.
     */
    public long getTraveler() {
        return traveler;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StatDailyTravelerKey that = (StatDailyTravelerKey) o;
        return getDateYear() == that.getDateYear() && getDateMonth() == that.getDateMonth() && getDateDay() == that.getDateDay() && getTraveler() == that.getTraveler();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDateYear(), getDateMonth(), getDateDay(), getTraveler());
    }
}
