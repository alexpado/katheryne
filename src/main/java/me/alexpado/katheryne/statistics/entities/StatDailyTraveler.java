package me.alexpado.katheryne.statistics.entities;

import me.alexpado.katheryne.statistics.entities.keys.StatDailyTravelerKey;
import me.alexpado.katheryne.statistics.entities.proxy.DailyTraveler;
import me.alexpado.katheryne.statistics.repositories.StatDailyTravelerRepository;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

/**
 * Immutable class providing data structure for {@link StatDailyTravelerRepository} and is never used directly in the
 * code.
 *
 * @author alexpado
 * @see DailyTraveler
 */
@Immutable
@Entity
@IdClass(StatDailyTravelerKey.class)
public class StatDailyTraveler {

    @Id
    private int  dateYear;
    @Id
    private int  dateMonth;
    @Id
    private int  dateDay;
    @Id
    private long traveler;

    private String tag;

    private long commissionAmount;

    private long autoClosed;

}
