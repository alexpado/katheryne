package me.alexpado.katheryne.statistics.entities.proxy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import me.alexpado.katheryne.commissions.entities.Commission;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.statistics.entities.interfaces.IStatDailyTraveler;
import me.alexpado.katheryne.statistics.repositories.StatDailyTravelerRepository;
import me.alexpado.katheryne.statistics.repositories.proxy.DailyTravelerProxy;

/**
 * @author alexpado
 * @see DailyTravelerProxy
 */
public class DailyTraveler implements IStatDailyTraveler {

    private final int    year;
    private final int    month;
    private final int    day;
    private final long   traveler;
    private final String tag;
    private final long   commissionAmount;
    private final long   autoClosed;

    /**
     * Create a new {@link DailyTraveler} instance using provided data. This constructor should only used by {@link
     * #flatMonth()} and {@link #flatYear()}.
     *
     * @param year
     *         The year of this {@link DailyTraveler}.
     * @param month
     *         The month of this {@link DailyTraveler}.
     * @param day
     *         The day of this {@link DailyTraveler}.
     * @param traveler
     *         The {@link Traveler#getId()} value of this {@link DailyTraveler}.
     * @param tag
     *         The {@link Traveler#getTag()} value of this {@link DailyTraveler}.
     * @param commissionAmount
     *         The {@link Commission} amount of this {@link DailyTraveler}.
     * @param autoClosed
     *         The amount of auto-closed {@link Commission} of this {@link DailyTraveler}.
     */
    public DailyTraveler(int year, int month, int day, long traveler, String tag, long commissionAmount, long autoClosed) {
        this.year             = year;
        this.month            = month;
        this.day              = day;
        this.traveler         = traveler;
        this.tag              = tag;
        this.commissionAmount = commissionAmount;
        this.autoClosed       = autoClosed;
    }

    /**
     * Create a new {@link DailyTraveler} instance using data from the provided {@link IStatDailyTraveler}. Most of the
     * time, this have to be used with proxy interface returned by {@link StatDailyTravelerRepository}.
     *
     * @param other
     *         The {@link IStatDailyTraveler} instance.
     */
    public DailyTraveler(IStatDailyTraveler other) {
        this.year             = other.getDateYear();
        this.month            = other.getDateMonth();
        this.day              = other.getDateDay();
        this.traveler         = other.getTraveler();
        this.tag              = other.getTag();
        this.commissionAmount = other.getCommissionAmount();
        this.autoClosed       = other.getAutoClosed();
    }

    /**
     * Create a new {@link DailyTraveler} setting the day value to <code>1</code>. This is useful when the date is
     * displayed for a monthly value.
     *
     * @return A copy of {@link DailyTraveler} with only its day value set to <code>1</code>.
     */
    @JsonIgnore
    public DailyTraveler flatMonth() {
        return new DailyTraveler(this.year, this.month, 1, this.traveler, this.tag, this.commissionAmount, this.autoClosed);
    }

    /**
     * Create a new {@link DailyTraveler} setting the day value and the month value to <code>1</code>. This is useful
     * when the date is displayed for a yearly value.
     *
     * @return A copy of {@link DailyTraveler} with only its day value and month value set to <code>1</code>.
     */
    @JsonIgnore
    public DailyTraveler flatYear() {
        return new DailyTraveler(this.year, 1, 1, this.traveler, this.tag, this.commissionAmount, this.autoClosed);
    }

    /**
     * Retrieves the year associated with this {@link IStatDailyTraveler}.
     *
     * @return The year
     */
    @Override
    @JsonIgnore
    public int getDateYear() {
        return this.year;
    }

    /**
     * Retrieves the month associated with this {@link IStatDailyTraveler}.
     *
     * @return The month
     */
    @Override
    @JsonIgnore
    public int getDateMonth() {
        return this.month;
    }

    /**
     * Retrieves the day associated with this {@link IStatDailyTraveler}.
     *
     * @return The day
     */
    @Override
    @JsonIgnore
    public int getDateDay() {
        return this.day;
    }

    /**
     * Retrieves the {@link Traveler}'s ID associated with this {@link IStatDailyTraveler}.
     *
     * @return The {@link Traveler}'s ID.
     */
    @Override
    public long getTraveler() {
        return this.traveler;
    }

    /**
     * Retrieves the {@link Traveler#getTag()} associated with this {@link IStatDailyTraveler}.
     *
     * @return The {@link Traveler#getTag()}.
     */
    @Override
    public String getTag() {
        return this.tag;
    }

    /**
     * Retrieves the {@link Commission} amount associated with this {@link IStatDailyTraveler}.
     *
     * @return The commission amount
     */
    @Override
    public long getCommissionAmount() {
        return this.commissionAmount;
    }

    /**
     * Retrieves the auto closed {@link Commission} amount associated with this {@link IStatDailyTraveler}.
     *
     * @return The auto closed commission amount
     */
    @Override
    public long getAutoClosed() {
        return this.autoClosed;
    }

}
