package me.alexpado.katheryne.statistics.entities.interfaces;

import me.alexpado.katheryne.commissions.entities.Commission;
import me.alexpado.katheryne.statistics.entities.StatDailyActivity;
import me.alexpado.katheryne.statistics.entities.proxy.DailyActivity;
import me.alexpado.katheryne.statistics.repositories.StatDailyActivityRepository;

import java.time.LocalDate;

/**
 * Interface used as proxy by {@link StatDailyActivityRepository}. This is because {@link StatDailyActivity} is based on
 * a view and is not compatible when using SUM and COUNT.
 *
 * A {@link LocalDate} can be built using {@link LocalDate#of(int, int, int)} with {@link #getDateYear()}, {@link
 * #getDateMonth()} and {@link #getDateDay()}.
 *
 * @author alexpado
 * @see DailyActivity
 */
public interface IStatDailyActivity {

    /**
     * Retrieves the year associated with this {@link IStatDailyActivity}.
     *
     * @return The year
     */
    int getDateYear();

    /**
     * Retrieves the month associated with this {@link IStatDailyActivity}.
     *
     * @return The month
     */
    int getDateMonth();

    /**
     * Retrieves the day associated with this {@link IStatDailyActivity}.
     *
     * @return The day
     */
    int getDateDay();

    /**
     * Retrieves the {@link Commission} amount associated with this {@link IStatDailyActivity}.
     *
     * @return The commission amount
     */
    long getCommissionAmount();

    /**
     * Retrieves the auto closed {@link Commission} amount associated with this {@link IStatDailyActivity}.
     *
     * @return The auto closed commission amount
     */
    long getAutoClosed();

}
