package me.alexpado.katheryne.statistics.entities;

import me.alexpado.katheryne.statistics.entities.keys.StatDailyActivityKey;
import me.alexpado.katheryne.statistics.entities.proxy.DailyActivity;
import me.alexpado.katheryne.statistics.repositories.StatDailyActivityRepository;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

/**
 * Immutable class providing data structure for {@link StatDailyActivityRepository} and is never used directly in the
 * code.
 *
 * @author alexpado
 * @see DailyActivity
 */
@Immutable
@Entity
@IdClass(StatDailyActivityKey.class)
public class StatDailyActivity {

    @Id
    private int dateYear;
    @Id
    private int dateMonth;
    @Id
    private int dateDay;

    private long commissionAmount;

    private long autoClosed;

}
