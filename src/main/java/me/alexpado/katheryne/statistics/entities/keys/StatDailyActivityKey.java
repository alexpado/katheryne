package me.alexpado.katheryne.statistics.entities.keys;

import me.alexpado.katheryne.statistics.entities.StatDailyActivity;

import javax.persistence.IdClass;
import java.io.Serializable;
import java.util.Objects;

/**
 * Class describing a {@link StatDailyActivity} ID. This class is meant to be used within an {@link IdClass} annotation,
 * even if there is no actual usage of {@link StatDailyActivity}.
 *
 * @author alexpado
 * @see StatDailyActivity
 */
public class StatDailyActivityKey implements Serializable {

    private int dateYear;
    private int dateMonth;
    private int dateDay;

    /**
     * Retrieves the year associated with this {@link StatDailyActivityKey}.
     *
     * @return The year
     */
    public int getDateYear() {
        return this.dateYear;
    }

    /**
     * Retrieves the month associated with this {@link StatDailyActivityKey}.
     *
     * @return The month
     */
    public int getDateMonth() {
        return this.dateMonth;
    }

    /**
     * Retrieves the day associated with this {@link StatDailyActivityKey}.
     *
     * @return The day
     */
    public int getDateDay() {
        return this.dateDay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StatDailyActivityKey that = (StatDailyActivityKey) o;
        return getDateYear() == that.getDateYear() && getDateMonth() == that.getDateMonth() && getDateDay() == that.getDateDay();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDateYear(), getDateMonth(), getDateDay());
    }
}
