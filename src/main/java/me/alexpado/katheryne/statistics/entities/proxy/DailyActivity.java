package me.alexpado.katheryne.statistics.entities.proxy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import me.alexpado.katheryne.commissions.entities.Commission;
import me.alexpado.katheryne.statistics.controllers.StatisticController;
import me.alexpado.katheryne.statistics.entities.interfaces.IStatDailyActivity;
import me.alexpado.katheryne.statistics.repositories.StatDailyActivityRepository;
import me.alexpado.katheryne.statistics.repositories.proxy.DailyActivityProxy;

import java.time.LocalDate;

/**
 * @author alexpado
 * @see DailyActivityProxy
 */
public class DailyActivity implements IStatDailyActivity {

    private final int  year;
    private final int  month;
    private final int  day;
    private final long commissionAmount;
    private final long autoClosed;

    /**
     * Create a new {@link DailyActivity} instance using provided data. This constructor should only used by {@link
     * #flatMonth()} and {@link #flatYear()}.
     *
     * @param year
     *         The year of this {@link DailyActivity}.
     * @param month
     *         The month of this {@link DailyActivity}.
     * @param day
     *         The day of this {@link DailyActivity}.
     * @param commissionAmount
     *         The {@link Commission} amount of this {@link DailyActivity}.
     * @param autoClosed
     *         The amount of auto-closed {@link Commission} of this {@link DailyActivity}.
     */
    private DailyActivity(int year, int month, int day, long commissionAmount, long autoClosed) {
        this.year             = year;
        this.month            = month;
        this.day              = day;
        this.commissionAmount = commissionAmount;
        this.autoClosed       = autoClosed;
    }

    /**
     * Create a new {@link DailyActivity} instance using data from the provided {@link IStatDailyActivity}. Most of the
     * time, this have to be used with proxy interface returned by {@link StatDailyActivityRepository}.
     *
     * @param other
     *         The {@link IStatDailyActivity} instance.
     */
    public DailyActivity(IStatDailyActivity other) {
        this.year             = other.getDateYear();
        this.month            = other.getDateMonth();
        this.day              = other.getDateDay();
        this.commissionAmount = other.getCommissionAmount();
        this.autoClosed       = other.getAutoClosed();
    }

    /**
     * Create a new {@link DailyActivity} setting the day value to <code>1</code>. This is useful when the date is
     * displayed for a monthly value.
     *
     * @return A copy of {@link DailyActivity} with only its day value set to <code>1</code>.
     */
    @JsonIgnore
    public DailyActivity flatMonth() {
        return new DailyActivity(this.year, this.month, 1, this.commissionAmount, this.autoClosed);
    }

    /**
     * Create a new {@link DailyActivity} setting the day value and the month value to <code>1</code>. This is useful
     * when the date is displayed for a yearly value.
     *
     * @return A copy of {@link DailyActivity} with only its day value and month value set to <code>1</code>.
     */
    @JsonIgnore
    public DailyActivity flatYear() {
        return new DailyActivity(this.year, 1, 1, this.commissionAmount, this.autoClosed);
    }

    /**
     * Retrieves the year associated with this {@link IStatDailyActivity}.
     *
     * @return The year
     */
    @Override
    @JsonIgnore
    public int getDateYear() {
        return this.year;
    }

    /**
     * Retrieves the month associated with this {@link IStatDailyActivity}.
     *
     * @return The month
     */
    @Override
    @JsonIgnore
    public int getDateMonth() {
        return this.month;
    }

    /**
     * Retrieves the day associated with this {@link IStatDailyActivity}.
     *
     * @return The day
     */
    @Override
    @JsonIgnore
    public int getDateDay() {
        return this.day;
    }

    /**
     * Retrieves the {@link Commission} amount associated with this {@link IStatDailyActivity}.
     *
     * @return The commission amount
     */
    @Override
    public long getCommissionAmount() {
        return this.commissionAmount;
    }

    /**
     * Retrieves the auto closed {@link Commission} amount associated with this {@link IStatDailyActivity}.
     *
     * @return The auto closed commission amount
     */
    @Override
    public long getAutoClosed() {
        return this.autoClosed;
    }

    /**
     * Retrieve the {@link LocalDate} of this {@link DailyActivity}. This is only, and should be only, used to display
     * the date in the JSON response given by {@link StatisticController}.
     *
     * @return The {@link LocalDate}.
     */
    @SuppressWarnings("unused")
    public LocalDate getDate() {
        return LocalDate.of(this.year, this.month, this.day);
    }
}
