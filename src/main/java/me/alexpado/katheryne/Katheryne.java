package me.alexpado.katheryne;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;

import java.util.Optional;

public final class Katheryne {

    public static final long ADVENTURER_GUILD_ID = 769533479133773853L;

    /**
     * Retrieving the optional {@link Guild} instance corresponding to the Adventurers' Guild server.
     *
     * @param jda
     *         The {@link JDA} instance.
     *
     * @return An optional {@link Guild}. May be empty if the {@link Guild} is not yet cached.
     */
    public static Optional<Guild> retrieveAdventurerGuild(JDA jda) {
        Guild guild = jda.getGuildById(Katheryne.ADVENTURER_GUILD_ID);
        return Optional.ofNullable(guild);
    }
}
