package me.alexpado.katheryne.messages.commissions;

import me.alexpado.katheryne.commissions.entities.Commission;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.JDA;

public class CommissionAutoClosedEmbed extends VendorEmbed {

    public CommissionAutoClosedEmbed(JDA jda, Commission commission) {

        super(jda);
        this.addField(commission.toField());
    }

}
