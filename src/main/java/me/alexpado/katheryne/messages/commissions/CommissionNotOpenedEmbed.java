package me.alexpado.katheryne.messages.commissions;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class CommissionNotOpenedEmbed extends VendorEmbed {

    public CommissionNotOpenedEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public CommissionNotOpenedEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setColor(Color.ORANGE);
        this.setDescription("Sorry Traveler, you do not have any opened commission.");
    }

}
