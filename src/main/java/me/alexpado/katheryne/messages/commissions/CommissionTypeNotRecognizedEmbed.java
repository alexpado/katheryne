package me.alexpado.katheryne.messages.commissions;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class CommissionTypeNotRecognizedEmbed extends VendorEmbed {

    public CommissionTypeNotRecognizedEmbed(ICommandContext context, String prefix) {
        super(context);
        this.configure(prefix);
    }

    public CommissionTypeNotRecognizedEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.configure("/");
    }

    private void configure(String prefix) {
        this.setColor(Color.ORANGE);
        this.appendDescription("Sorry Traveler, but I don't recognize this type of commission.\n");
        this.appendDescription("Type `").appendDescription(prefix).appendDescription("commission help` to get more info.");
    }
}
