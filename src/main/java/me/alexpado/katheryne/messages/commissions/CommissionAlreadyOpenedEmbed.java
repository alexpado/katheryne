package me.alexpado.katheryne.messages.commissions;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class CommissionAlreadyOpenedEmbed extends VendorEmbed {

    public CommissionAlreadyOpenedEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public CommissionAlreadyOpenedEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setColor(Color.ORANGE);
        this.appendDescription("Sorry Traveler,\n\n");
        this.appendDescription("You already have a commission opened.\n");
        this.appendDescription("Only one commission per Traveler can be opened at the same time.");
    }
}
