package me.alexpado.katheryne.messages.commissions;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class CommissionOpenedEmbed extends VendorEmbed {

    public CommissionOpenedEmbed(ICommandContext context, boolean showDirectMessageWarning) {
        super(context);
        this.configure(showDirectMessageWarning);
    }

    public CommissionOpenedEmbed(Interaction event, boolean showDirectMessageWarning) {
        super(event.getJDA(), event.getUser());
        this.configure(showDirectMessageWarning);
    }

    private void configure(boolean showDirectMessageWarning) {

        this.setColor(Color.GREEN);
        this.appendDescription("Your commission is now opened.");

        if (showDirectMessageWarning) {
            this.appendDescription("\n");
            this.appendDescription("As your UID won't be visible for this commission, you should check that everyone can DM you.");
        }
    }

}
