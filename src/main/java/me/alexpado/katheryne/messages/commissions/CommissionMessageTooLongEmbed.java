package me.alexpado.katheryne.messages.commissions;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class CommissionMessageTooLongEmbed extends VendorEmbed {

    public CommissionMessageTooLongEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public CommissionMessageTooLongEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setColor(Color.RED);
        this.setDescription("Sorry Traveler, your message exceed the 255 characters limit.");
    }

}
