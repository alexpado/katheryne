package me.alexpado.katheryne.messages.commissions;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class FillProfileErrorEmbed extends VendorEmbed {

    public FillProfileErrorEmbed(ICommandContext context, String prefix) {
        super(context);

        this.setColor(Color.RED);
        this.setDescription("Sorry Traveler, you need to fill your profile first with **at least** your world level.");
        this.addField("Setting your world level", String.format("`%sprofile %s`", prefix, "[region] set wl [level]"), false);
    }

    public FillProfileErrorEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());

        this.setColor(Color.RED);
        this.setDescription("Sorry Traveler, you need to fill your profile first with **at least** your world level.");
        this.setDescription("Please use the `/profile update` command.");
    }
}
