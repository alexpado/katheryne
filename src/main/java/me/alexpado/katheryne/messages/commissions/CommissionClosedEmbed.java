package me.alexpado.katheryne.messages.commissions;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class CommissionClosedEmbed extends VendorEmbed {

    public CommissionClosedEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public CommissionClosedEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setColor(Color.GREEN);
        this.appendDescription("Your commission is now closed.\n");
        this.appendDescription("Don't forget to drop a 'Thanks' to the Traveler(s) who helped you !");
    }

}
