package me.alexpado.katheryne.messages.commissions;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.genshin.entities.Restriction;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TravelerRestrictedEmbed extends VendorEmbed {

    public TravelerRestrictedEmbed(ICommandContext context, Restriction restriction) {
        super(context);
        this.configure(restriction);
    }

    public TravelerRestrictedEmbed(Interaction event, Restriction restriction) {
        super(event.getJDA(), event.getUser());
        this.configure(restriction);
    }

    private void configure(Restriction restriction) {
        this.setColor(Color.BLACK);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm a (HH:mm)");
        this.setDescription("Sorry Traveler, you can't post a new commission because you have been restricted.");
        this.addField("Reason", restriction.getReason(), false);
        this.addField("Starting from", restriction.getStartingAt().format(dateTimeFormatter), true);

        if (restriction.getEndingAt() != LocalDateTime.MAX) {
            this.addField("Ending at", restriction.getEndingAt().format(dateTimeFormatter), true);
        } else {
            this.addField("Ending at", "*Never...*", true);
        }
    }

}
