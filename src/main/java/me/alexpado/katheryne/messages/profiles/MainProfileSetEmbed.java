package me.alexpado.katheryne.messages.profiles;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class MainProfileSetEmbed extends VendorEmbed {

    public MainProfileSetEmbed(ICommandContext context, ServerRegion region) {
        super(context);
        this.configure(region);
    }

    public MainProfileSetEmbed(Interaction event, ServerRegion region) {
        super(event.getJDA(), event.getUser());
        this.configure(region);
    }

    private void configure(ServerRegion region) {
        this.setColor(Color.GREEN);
        this.appendDescription(String.format("Your main region is now set to %s.", region.name().toLowerCase()));
    }
}
