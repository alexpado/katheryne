package me.alexpado.katheryne.messages.profiles;

import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class NotEnoughSearchOptionsEmbed extends VendorEmbed {

    public NotEnoughSearchOptionsEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setDescription("Please use one option when searching for a profile.");
        this.setColor(Color.ORANGE);
    }
}
