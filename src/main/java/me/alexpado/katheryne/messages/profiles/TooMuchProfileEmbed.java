package me.alexpado.katheryne.messages.profiles;

import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class TooMuchProfileEmbed extends VendorEmbed {

    public TooMuchProfileEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setDescription("This UID matched more than one profile. Two or more Travelers are using the same UID, please report this using the feedback feature.");
        this.setColor(Color.GREEN);
    }
}
