package me.alexpado.katheryne.messages.profiles;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class FullUpdatedEmbed extends VendorEmbed {

    public FullUpdatedEmbed(Interaction event, ServerRegion region) {
        super(event.getJDA(), event.getUser());
        this.configure(region);
    }

    private void configure(ServerRegion region) {
        this.setDescription(String.format("Your world level and UID for the region %s have been updated.", region.name()));
        this.setColor(Color.GREEN);
    }
}
