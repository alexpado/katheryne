package me.alexpado.katheryne.messages.profiles;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class ProfileNotFoundEmbed extends VendorEmbed {

    public ProfileNotFoundEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public ProfileNotFoundEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setDescription("Sorry Traveler, I couldn't found the profile you are looking for :(");
        this.setColor(Color.ORANGE);
    }

}
