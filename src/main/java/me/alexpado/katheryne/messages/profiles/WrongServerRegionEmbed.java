package me.alexpado.katheryne.messages.profiles;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class WrongServerRegionEmbed extends VendorEmbed {

    public WrongServerRegionEmbed(ICommandContext context, boolean showAll) {
        super(context);
        this.configure(showAll);
    }

    public WrongServerRegionEmbed(Interaction event, boolean showAll) {
        super(event.getJDA(), event.getUser());
        this.configure(showAll);
    }

    private void configure(boolean showAll) {
        this.appendDescription("Unknown server. Valid server: `na`, `eu`, `asia`, `sar`.");
        if (showAll) {
            this.appendDescription("\n\n");
            this.appendDescription("You can also select all of them using `all`.");
        }

        this.setColor(Color.RED);
    }
}
