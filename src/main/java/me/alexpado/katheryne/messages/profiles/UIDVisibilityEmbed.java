package me.alexpado.katheryne.messages.profiles;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class UIDVisibilityEmbed extends VendorEmbed {

    public UIDVisibilityEmbed(ICommandContext context, Traveler traveler) {
        super(context);
        this.configure(traveler);
    }

    public UIDVisibilityEmbed(Interaction event, Traveler traveler) {
        super(event.getJDA(), event.getUser());
        this.configure(traveler);
    }

    private void configure(Traveler traveler) {
        if (traveler.isUidHidden()) {
            this.setDescription("Your UID is now private.\nPlease note that may discourage some people to participate to your commissions.");
        } else {
            this.setDescription("Your UID is now public.");
        }

        this.setColor(Color.GREEN);
    }

}
