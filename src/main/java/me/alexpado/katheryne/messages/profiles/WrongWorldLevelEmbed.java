package me.alexpado.katheryne.messages.profiles;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;

import java.awt.*;

public class WrongWorldLevelEmbed extends VendorEmbed {

    public WrongWorldLevelEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    private void configure() {
        this.setDescription("Please input a valid WL.");
        this.setColor(Color.RED);
    }
}
