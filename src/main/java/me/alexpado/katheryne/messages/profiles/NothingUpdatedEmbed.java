package me.alexpado.katheryne.messages.profiles;

import me.alexpado.katheryne.genshin.enums.ServerRegion;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class NothingUpdatedEmbed extends VendorEmbed {

    public NothingUpdatedEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setDescription("No changes were made. Please provide at least the uid or the world level to update your profile.");
        this.setColor(Color.ORANGE);
    }
}
