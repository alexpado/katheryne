package me.alexpado.katheryne.messages.profiles;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class NoMatchedProfileEmbed extends VendorEmbed {

    public NoMatchedProfileEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public NoMatchedProfileEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setColor(Color.RED);
        this.appendDescription("Sorry Traveler, but I'm unable to find any configured profile in this region.");
    }
}
