package me.alexpado.katheryne.messages.profiles;

import me.alexpado.katheryne.genshin.enums.ServerRegion;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class TooMuchSearchOptionsEmbed extends VendorEmbed {

    public TooMuchSearchOptionsEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setDescription("Please only use one option when searching for a profile.");
        this.setColor(Color.ORANGE);
    }
}
