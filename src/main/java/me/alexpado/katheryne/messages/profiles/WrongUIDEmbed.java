package me.alexpado.katheryne.messages.profiles;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class WrongUIDEmbed extends VendorEmbed {

    public WrongUIDEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public WrongUIDEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setDescription("The UID provided isn't compatible with the region provided.");
        this.setColor(Color.RED);
    }
}
