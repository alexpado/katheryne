package me.alexpado.katheryne.messages.profiles;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class WorldLevelUpdatedEmbed extends VendorEmbed {

    public WorldLevelUpdatedEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public WorldLevelUpdatedEmbed(Interaction event, ServerRegion region) {
        super(event.getJDA(), event.getUser());
        this.configure(region);
    }

    private void configure() {
        this.setDescription("Your WL has been updated.");
        this.setColor(Color.GREEN);
    }


    private void configure(ServerRegion region) {
        this.setDescription(String.format("Your world level for the region %s has been updated.", region.name()));
    }
}
