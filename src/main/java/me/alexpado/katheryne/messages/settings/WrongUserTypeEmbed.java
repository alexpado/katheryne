package me.alexpado.katheryne.messages.settings;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;

import java.awt.*;

public class WrongUserTypeEmbed extends VendorEmbed {

    public WrongUserTypeEmbed(ICommandContext context) {
        super(context);
        this.setDescription("Oh ? I can't be activated by this kind of users. Please try again using an user account.");
        this.setColor(Color.RED);
    }
}
