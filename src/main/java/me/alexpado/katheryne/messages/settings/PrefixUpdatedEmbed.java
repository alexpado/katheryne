package me.alexpado.katheryne.messages.settings;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;

import java.awt.*;

public class PrefixUpdatedEmbed extends VendorEmbed {

    public PrefixUpdatedEmbed(ICommandContext context) {
        super(context);
        this.setDescription("Your server's prefix has been updated !");
        this.setColor(Color.GREEN);
    }
}
