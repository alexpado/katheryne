package me.alexpado.katheryne.messages.settings;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;

import java.awt.*;

public class InvalidPrefixEmbed extends VendorEmbed {

    public InvalidPrefixEmbed(ICommandContext context) {
        super(context);
        this.setDescription("Sorry Traveler, but the prefix you entered is invalid.");
        this.setColor(Color.RED);
    }
}
