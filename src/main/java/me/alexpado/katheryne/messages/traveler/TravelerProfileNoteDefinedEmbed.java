package me.alexpado.katheryne.messages.traveler;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class TravelerProfileNoteDefinedEmbed extends VendorEmbed {

    public TravelerProfileNoteDefinedEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public TravelerProfileNoteDefinedEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {

        this.setColor(Color.GREEN);
        this.setDescription("Your profile's note has been edited.");
    }
}
