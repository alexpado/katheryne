package me.alexpado.katheryne.messages;

import fr.alexpado.jda.DiscordBotImpl;
import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.GenshinCoopApplication;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.User;

public class VendorEmbed extends EmbedBuilder {

    public VendorEmbed(JDA jda, User user) {
        this.prepare(jda, user);
    }

    @Deprecated
    public VendorEmbed(ICommandContext context) {
        this.prepare(context.getEvent().getJDA(), context.getEvent().getAuthor());
    }

    public VendorEmbed(JDA jda) {
        this.prepare(jda);
    }

    public void prepare(JDA jda) {
        long   permission = Permission.getRaw(Permission.MESSAGE_SEND, Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_MANAGE);
        long   clientId   = jda.getSelfUser().getIdLong();
        String inviteLink = String.format(GenshinCoopApplication.INV_LINK, clientId, permission);
        this.setAuthor("Click here to add me to your server !", inviteLink, jda.getSelfUser().getAvatarUrl());
        this.setFooter(String.format("%s • Powered by the Adventurers' Guild", jda.getSelfUser().getName()));
    }

    public void prepare(JDA jda, User user) {
        this.prepare(jda);
        this.setFooter(String.format("%s#%s — %s • Powered by the Adventurers' Guild", user.getName(), user.getDiscriminator(), jda.getSelfUser().getName()), user.getAvatarUrl());
    }

}
