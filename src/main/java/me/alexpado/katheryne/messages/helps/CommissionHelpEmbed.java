package me.alexpado.katheryne.messages.helps;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;

public class CommissionHelpEmbed extends VendorEmbed {


    public CommissionHelpEmbed(ICommandContext context) {
        super(context);

        this.appendDescription("To create a commission, you can use the command `coop@commission create [type] message...`.\n");
        this.appendDescription("For example, if you want to get help to beat the Oceanid, you could write:\n");
        this.appendDescription("```coop@commission create boss Hi, need help to defeat the Oceanid 3 times !```\n");
        this.appendDescription("Please do note that every commission type have also aliases, if you're lazy ! (syntax: `coop@c-type message...`)\n\n");
        this.appendDescription("Example:\n");
        this.appendDescription("```coop@c-boss Hi, need help to defeat the Oceanid 3 times !```\n");
        this.appendDescription("Posting a commission will be done under your default profile.\n");
        this.appendDescription("Please double check your default profile before posting !\n\n");
        this.appendDescription(":mag: Here is a list of commission type available :\n");
        this.appendDescription("──────────────────────");

        this.addField("boss", "Doing bosses like Oceanid, Regisvine, Hypostasis, Wolf, Childe...", false)
                .addField("bounty", "If your bounties are a little bit too invulnerable to your team...", false)
                .addField("domain", "Go loot that artifact you're trying to loot for ~~days~~ weeks now.", false)
                .addField("mob", "Hilichurls, Slime, Fatui... kill 'em all !", false)
                .addField("resource", "Mining or getting some pretty flower, what do you want ?", false)
                .addField("challenge", "Always missing 1s to your challenge ? Make it 2 by inviting more people !", false)
                .addField("event", "Because events are way more fun in coop", false)
                .addField("wei", "*Ya odomu, Todo yo, buka guru-guru nye !*... Wait. Wrong script.", false)
                .addField("teapot", "If you feel lonely is your house !", false)
                .addField("other", "Well, if nothing fits, describe your issue with the message !", false);
    }
}
