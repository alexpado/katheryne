package me.alexpado.katheryne.messages.helps;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;

public class BoardHelpEmbed extends VendorEmbed {

    public BoardHelpEmbed(ICommandContext context, IDiscordBot bot) {
        super(context);
        this.appendDescription(":mag: Here is a list of available commands to manage this server's commissions boards:\n");
        this.appendDescription("Possible values for region: `EU`, `NA`, `SAR`, `ASIA` and `ALL`.\n");
        this.appendDescription("──────────────────────");

        this.addCommand(context, bot, "[channel] create [region]", "Create a new board in the provided channel with the specified region.");
        this.addCommand(context, bot, "[channel] remove", "Remove the board present in the provided channel.");
        this.addCommand(context, bot, "[channel] notification [region] change message...", "Change the board's notification message for the provided region. Using `ALL` will affect all regions (and is only useful if `ALL` as been used when the board was created).");
        this.addCommand(context, bot, "[channel] notification [region] trigger", "Trigger the board's notification for the provided region. Using `ALL` will affect all regions (and is only useful if `ALL` as been used when the board was created).");
        this.addCommand(context, bot, "[channel]", "Display the board's settings.");
    }

    public void addCommand(ICommandContext context, IDiscordBot bot, String cmd, String desc) {

        this.addField(this.toCommand(context, bot, cmd), desc, false);
    }

    private String toCommand(ICommandContext context, IDiscordBot bot, String label) {

        return bot.getCommandHelper().getApplicablePrefix(context.getEvent()) + "board " + label;
    }
}
