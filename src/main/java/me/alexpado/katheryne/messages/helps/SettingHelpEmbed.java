package me.alexpado.katheryne.messages.helps;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;

public class SettingHelpEmbed extends VendorEmbed {

    public SettingHelpEmbed(ICommandContext context, IDiscordBot bot) {
        super(context);
        this.appendDescription("This sets of command require special permissions.\n");
        this.appendDescription("\n");
        this.appendDescription(":key: Require admin or manage server permissions.\n");
        this.appendDescription(":closed_lock_with_key: Require bot admin privileges.\n");
        this.appendDescription("──────────────────────");

        this.addCommand(context, bot, "server prefix [prefix]", ":key: Define this server command prefix.");
        this.addCommand(context, bot, "server toggle vip", ":closed_lock_with_key: Switch the vip state for the current server.");
    }


    public void addCommand(ICommandContext context, IDiscordBot bot, String cmd, String desc) {

        this.addField(this.toCommand(context, bot, cmd), desc, false);
    }

    private String toCommand(ICommandContext context, IDiscordBot bot, String label) {

        return bot.getCommandHelper().getApplicablePrefix(context.getEvent()) + "settings " + label;
    }
}
