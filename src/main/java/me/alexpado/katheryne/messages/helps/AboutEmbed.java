package me.alexpado.katheryne.messages.helps;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.interactions.Interaction;

public class AboutEmbed extends VendorEmbed {

    public AboutEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public AboutEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {

        this.appendDescription("**Ad Astra Abyssosque !**\n");
        this.appendDescription("Welcome to the Adventurers' Guild.");
        this.appendDescription("\n──────────────────────\n\n");
        this.appendDescription("This bot was made by `Akio Nakao#0001`.\n");
        this.appendDescription("Huge thanks to `Sandy#7777` & `Itchybug#9023` for their continuous help and support.\n\n");
        this.appendDescription("Special thanks to the **Katheryne Mains** server for their trust and help to get the bot started.\n\n");
        this.appendDescription("Thanks to all contributors who gave valuable feedbacks and helped on many bugfixes and QoL updates.\n\n");
        this.appendDescription("You can also join the Adventurers' Guild server by [clicking here](https://discord.com/invite/b9SggV5vqU)");

    }
}
