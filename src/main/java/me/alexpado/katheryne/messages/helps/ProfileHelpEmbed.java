package me.alexpado.katheryne.messages.helps;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;

public class ProfileHelpEmbed extends VendorEmbed {

    public ProfileHelpEmbed(ICommandContext context, IDiscordBot bot) {
        super(context);
        this.appendDescription(":mag: Here is a list of available commands to update or view a profile:\n");
        this.appendDescription("Possible values for region: `EU`, `NA`, `SAR` and `ASIA`.\n");
        this.appendDescription("──────────────────────");

        this.addCommand(context, bot, true, "set uid [UID]", "Define your UID.");
        this.addCommand(context, bot, true, "set wl [WL]", "Define your WL.");
        this.addCommand(context, bot, true, "set main", "Define your main region.");
        this.addCommand(context, bot, false, "view", "Display your profile.");
        this.addCommand(context, bot, false, "view [tag/mention]", "Display a Traveler's profile. You can mention a Traveler or use its tag (ex: `Akio Nakao#0001`)");
        this.addCommand(context, bot, false, "make uid private", "Hide your UID from your profile.");
        this.addCommand(context, bot, false, "make uid public", "Display your UID on your profile.");
    }

    public void addCommand(ICommandContext context, IDiscordBot bot, boolean regionSpecific, String cmd, String desc) {

        if (regionSpecific) {
            this.addField(this.toRegionCommand(context, bot, cmd), desc, false);
        } else {
            this.addField(this.toCommand(context, bot, cmd), desc, false);
        }
    }

    private String toCommand(ICommandContext context, IDiscordBot bot, String label) {

        return bot.getCommandHelper().getApplicablePrefix(context.getEvent()) + "profile " + label;
    }

    private String toRegionCommand(ICommandContext context, IDiscordBot bot, String label) {

        return bot.getCommandHelper().getApplicablePrefix(context.getEvent()) + "profile [region] " + label;
    }
}
