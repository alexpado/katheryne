package me.alexpado.katheryne.messages.helps;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.commands.interfaces.ICommand;
import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import fr.alexpado.jda.services.commands.interfaces.ICommandMeta;
import me.alexpado.katheryne.messages.VendorEmbed;

import java.util.Collection;
import java.util.stream.Collectors;

public class GeneralHelpEmbed extends VendorEmbed {


    public GeneralHelpEmbed(ICommandContext context, IDiscordBot bot) {
        super(context);
        String               prefix   = bot.getCommandHelper().getApplicablePrefix(context.getEvent());
        Collection<ICommand> commands = bot.getCommandHandler().getCommands().values();

        Collection<ICommand> displayable = commands.stream()
                .filter(cmd -> cmd.getMeta().getDescription() != null).toList();

        this.appendDescription("**Ad Astra Abyssosque !**\n");
        this.appendDescription("Welcome to the Adventurers' Guild.");
        this.appendDescription("\n──────────────────────\n");
        this.appendDescription("Each command have their own help section, as listed below.\n");


        for (ICommand command : displayable) {
            ICommandMeta meta = command.getMeta();

            this.addField(String.format("%s%s help", prefix, meta.getLabel()), meta.getDescription(), false);
        }
    }
}
