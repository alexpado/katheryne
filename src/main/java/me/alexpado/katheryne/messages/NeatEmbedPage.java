package me.alexpado.katheryne.messages;

import fr.alexpado.jda.interfaces.IDiscordBot;
import fr.alexpado.jda.services.tools.embed.EmbedPage;
import me.alexpado.katheryne.discord.interfaces.Fieldable;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.util.List;

public class NeatEmbedPage<T extends Fieldable> extends EmbedPage<T> {

    private final String      title;
    private final IDiscordBot bot;

    public NeatEmbedPage(IDiscordBot bot, List<T> items, int timeout, String title) {

        super(items, timeout);
        this.title = title;
        this.bot   = bot;
    }

    @Override
    public EmbedBuilder getEmbed(JDA jda) {

        EmbedBuilder builder = this.bot.getCommandHelper().getVendorEmbed(jda);
        builder.setDescription(String.format(":mag: %s:\n──────────────────────", this.title));

        return builder;
    }

    @Override
    public MessageEmbed.Field getFieldFor(int index, T item) {

        return item.toField();
    }
}
