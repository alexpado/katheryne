package me.alexpado.katheryne.messages.base;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.genshin.entities.Profile;
import me.alexpado.katheryne.genshin.entities.Traveler;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.util.List;

public class TravelerProfile extends VendorEmbed {

    public TravelerProfile(ICommandContext context, Traveler traveler, List<Profile> profiles) {

        super(context);
        this.configure(traveler, profiles);
    }

    public TravelerProfile(Interaction event, Traveler traveler, List<Profile> profiles) {
        super(event.getJDA(), event.getUser());
        this.configure(traveler, profiles);
    }

    private void configure(Traveler traveler, List<Profile> profiles) {
        this.setDescription(this.getProfileLine(traveler));
        this.createProfileBlock(profiles);
    }

    private String getProfileLine(Traveler traveler) {

        if (traveler.getNote() != null) {
            return String.format("Profile for `%s#%s`:\n\n**About %s**\n%s", traveler.getName(), traveler.getDiscriminator(), traveler.getName(), traveler.getNote());
        } else {
            return String.format("Profile for `%s#%s`", traveler.getName(), traveler.getDiscriminator());
        }

    }

    private void createProfileBlock(List<Profile> profiles) {

        for (Profile profile : profiles) {

            String activityEmote = profile.isMain() ? "\uD83D\uDFE2" : "\uD83D\uDFE1";

            String title = String.format("%s • %s server", activityEmote, profile.getRegion().name().toLowerCase());
            String desc  = String.format("WL %s • UID %s", profile.getPublicWorldLevel(), profile.getPublicUid(false));

            this.addField(title, desc, false);
        }
    }


}
