package me.alexpado.katheryne.messages.base;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;

import java.awt.*;

public class DeprecatedCommandEmbed extends VendorEmbed {

    public DeprecatedCommandEmbed(ICommandContext context) {
        super(context);

        this.setColor(Color.ORANGE);
        this.appendDescription("Sorry Traveler, this is the old command syntax.\n");
        this.appendDescription("Please review the help menu to check the new syntaxes.");
    }
}
