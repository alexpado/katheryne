package me.alexpado.katheryne.messages.base;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class NotEnoughPermissionEmbed extends VendorEmbed {

    public NotEnoughPermissionEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public NotEnoughPermissionEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setDescription("You do not have the permission to do this.");
        this.setColor(Color.RED);
    }
}
