package me.alexpado.katheryne.messages.restrict;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class SelfRestrictErrorEmbed extends VendorEmbed {

    public SelfRestrictErrorEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public SelfRestrictErrorEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setDescription("You can't restrict another adm-...\nWait, did you really tried to restrict yourself ?!");
        this.setColor(Color.RED);
    }
}
