package me.alexpado.katheryne.messages.restrict;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class AdminRestrictErrorEmbed extends VendorEmbed {

    public AdminRestrictErrorEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public AdminRestrictErrorEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setDescription("You can't restrict another admin.");
        this.setColor(Color.RED);
    }
}
