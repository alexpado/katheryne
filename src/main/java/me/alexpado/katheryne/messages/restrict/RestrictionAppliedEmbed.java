package me.alexpado.katheryne.messages.restrict;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class RestrictionAppliedEmbed extends VendorEmbed {

    public RestrictionAppliedEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public RestrictionAppliedEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setDescription("The restriction has been applied.");
        this.setColor(Color.YELLOW);
    }
}
