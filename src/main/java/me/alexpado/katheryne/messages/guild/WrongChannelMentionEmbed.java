package me.alexpado.katheryne.messages.guild;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

public class WrongChannelMentionEmbed extends VendorEmbed {

    public WrongChannelMentionEmbed(ICommandContext context) {
        super(context);
        this.setDescription("Sorry, but I didn't recognized this channel.");
    }

    public WrongChannelMentionEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.setDescription("Sorry, but this channel isn't compatible.");
    }
}
