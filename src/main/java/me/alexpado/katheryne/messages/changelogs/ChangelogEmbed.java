package me.alexpado.katheryne.messages.changelogs;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

public class ChangelogEmbed extends VendorEmbed {

    public ChangelogEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public ChangelogEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {

        this.addChange(
                "Slash Commands are here !",
                """
                        This is a new feature introduced by Discord to help user and bot developer with commands. Slash commands work similar to the ones you're used to. Now, if you type `/` in your server, you'll see all Katheryne commands with their descriptions !
                        Make sure to familiarize yourself with this feature as normal commands will be soon removed !

                        * *It may take up to one hour after the update for commands to show up in this list.*
                        """
        );

        this.addChange(
                "But wait, there's more !",
                "Slash Command allows you to use commands with Katheryne's DM ! Obviously, some commands won't be available such as board, as it need a server channel to work."
        );

        this.addChange(
                "QoL: Easy to DM",
                "Now for each commission, you will have a link that will open the DM channel with the Traveler who posted the commission ! Nice !"
        );

        this.addChange(
                "QoL: No more region posting hell !",
                """
                        Tired of changing your main profile to post a commission in a specific region ? Say no more ! You can now post a commission in a region without changing your main profile !

                        * *Only available with Slash Commands though... Sorry !*"""
        );

        this.addChange(
                "Message of the day !",
                """
                        Tell all Traveler how you're feeling today by adding a little note to your profile !

                        * `/traveler note set` and `/traveler note remove` (also available through old command system, but don't use them anymore, please)"""
        );

        this.addChange("Deleted `bug`, `ask` and `idea` commands !",
                "But don't worry too much! Continue reading...");

        this.addChange("About the adventurers' guild...",
                "The guild's building is now finished! You can get the invitation link to join our server by using the command `/about`"
        );
    }

    private void addChange(String title, String description) {

        this.appendDescription("\uD83C\uDF63 **").appendDescription(title).appendDescription("**\n");

        String desc = "> ";
        if (description.contains("\n")) {
            desc += String.join("\n> ", description.split("\n"));
        } else {
            desc += description;
        }
        this.appendDescription(desc);
        this.appendDescription("\n\n");
    }
}
