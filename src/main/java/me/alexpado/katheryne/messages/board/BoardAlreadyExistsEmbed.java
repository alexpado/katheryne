package me.alexpado.katheryne.messages.board;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class BoardAlreadyExistsEmbed extends VendorEmbed {

    public BoardAlreadyExistsEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public BoardAlreadyExistsEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setColor(Color.ORANGE);
        this.appendDescription("It seems that a board already exists in the provided channel.\n\n");
        this.appendDescription("Please choose another channel or remove the existing one.");
    }

}
