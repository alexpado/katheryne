package me.alexpado.katheryne.messages.board;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class BoardAddedEmbed extends VendorEmbed {

    public BoardAddedEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public BoardAddedEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setColor(Color.GREEN);
        this.appendDescription("The board has been created.");
    }
}
