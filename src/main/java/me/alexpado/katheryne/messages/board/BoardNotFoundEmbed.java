package me.alexpado.katheryne.messages.board;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class BoardNotFoundEmbed extends VendorEmbed {

    public BoardNotFoundEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public BoardNotFoundEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setColor(Color.GREEN);
        this.appendDescription("Sorry, but I'm unable to find a board in the provided channel.");
    }
}
