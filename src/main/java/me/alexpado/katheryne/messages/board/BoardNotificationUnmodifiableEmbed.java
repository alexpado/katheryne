package me.alexpado.katheryne.messages.board;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class BoardNotificationUnmodifiableEmbed extends VendorEmbed {

    public BoardNotificationUnmodifiableEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public BoardNotificationUnmodifiableEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setColor(Color.ORANGE);
        this.appendDescription("Unable to execute your command with current settings: The region provided isn't compatible with the current board's settings.\n\n");
        this.appendDescription("You may have to remove the board and re-create it with the new region.");
    }
}
