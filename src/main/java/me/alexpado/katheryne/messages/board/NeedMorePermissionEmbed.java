package me.alexpado.katheryne.messages.board;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class NeedMorePermissionEmbed extends VendorEmbed {

    public NeedMorePermissionEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public NeedMorePermissionEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setColor(Color.MAGENTA);
        this.setDescription("Sorry Traveler, I don't have enough permission to do what you want on that channel. Here's what I need:");
        this.addField("Send Messages", "This should be obvious, but sometime by wanting to restrict access to a lot of people, this can be overlooked.", false);
        this.addField("Embed Links", "This is needed to create the board. It will be displayed almost like a link preview, but that's not a link.", false);
    }
}
