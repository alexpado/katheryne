package me.alexpado.katheryne.messages.board;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;

public class BoardInaccessibleEmbed extends VendorEmbed {

    public BoardInaccessibleEmbed(ICommandContext context) {
        super(context);
        this.configure();
    }

    public BoardInaccessibleEmbed(Interaction event) {
        super(event.getJDA(), event.getUser());
        this.configure();
    }

    private void configure() {
        this.setColor(Color.ORANGE);
        this.appendDescription("A board has been found but is inaccessible.\n\n");
        this.appendDescription("Please check the channel permissions and try again.");
    }
}
