package me.alexpado.katheryne.messages.board;

import fr.alexpado.jda.services.commands.interfaces.ICommandContext;
import me.alexpado.katheryne.commissions.entities.Board;
import me.alexpado.katheryne.commissions.entities.Notification;
import me.alexpado.katheryne.genshin.enums.ServerRegion;
import me.alexpado.katheryne.messages.VendorEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.Interaction;

import java.awt.*;
import java.util.List;


public class BoardDetailsEmbed extends VendorEmbed {

    public BoardDetailsEmbed(ICommandContext context, Board board, List<Notification> notifications) {
        super(context);
        this.configure(board, notifications);
    }

    public BoardDetailsEmbed(Interaction event, Board board, List<Notification> notifications) {
        super(event.getJDA(), event.getUser());
        this.configure(board, notifications);
    }

    private void configure(Board board, List<Notification> notifications) {
        this.setColor(Color.CYAN);

        if (board.getRegion() == ServerRegion.ALL) {
            this.setDescription("This board will display commissions from all regions.");
        } else {
            String boardDetailFormat = "This board will display commissions from `%s` region.";
            this.setDescription(String.format(boardDetailFormat, board.getRegion().name().toLowerCase()));
        }

        for (Notification notification : notifications) {
            String title = String.format("Notification for %s", notification.getRegion());
            this.addField(title, notification.getEffectiveContent(), false);
        }
    }

}
