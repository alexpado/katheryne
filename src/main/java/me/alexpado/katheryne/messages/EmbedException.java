package me.alexpado.katheryne.messages;

import fr.alexpado.jda.services.commands.exceptions.FriendlyException;
import net.dv8tion.jda.api.EmbedBuilder;

public class EmbedException extends FriendlyException {

    private final EmbedBuilder embedBuilder;

    public EmbedException(EmbedBuilder embedBuilder) {
        super(null);
        this.embedBuilder = embedBuilder;
    }

    @Override
    public EmbedBuilder toEmbed() {
        return this.embedBuilder;
    }
}
