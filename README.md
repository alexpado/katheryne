# Katheryne Bot

The bot that make coop in Genshin Impact easier.

## Goal

The goal of this bot is to make coop between servers possible. Without numbers, we can tell that a lot of players
actually created their own Discord server related (directly or indirectly)
to Genshin Impact.

This bot allows coop between these servers by providing a way for players to post commission that will be saved within
the bot, readable by anyone who have access to the bot.

## The Traveler's Profile

This is the first and required step before posting any commission.

A Traveler's Profile is composed by four information:

- Server region (*NA/EU/ASIA/SAR*)
- World Level
- Adventure Rank
- UID

Although only the server region and world level are required to post any commission, this is highly recommended filling
in everything to make the life easier for everyone.

**NOTE:** If it's your first time using the bot, your UID will be private by default. You can choose its visibility with
commands. Nothing can happen by setting your UID as public, except getting other Traveler help faster than ever.

## The commission system.

A Traveler (after filling in its profile) can post commission with the bot. Other Traveler will be able to list all
opened commission to help.

A commission displayed on screen will contain:

- The Traveler's server region
- The Traveler's world level
- The Traveler's Discord Tag (*eg: Akio Nakao#0001*)
- The Commission's type (*eg: Boss*)
- The Commission's details

> Why my Discord Tag is shown ?

As a lot of information can be displayed on screen, showing the profile of each individual Travelers can take some
space. Using the Discord Tag to identify a Traveler, you can display its profile using it (please refer to the commands
section).

> So my UID isn't required right ?

Absolutely, and that's why it's not required to post commission. It would be easier though to be able to see your UID
within the bot to avoid having to add you as friend on Discord to ask for UID.

If you're not providing your UID, please at least be sure that we can contact you by DM.

## Commands

**Setting your profile's server region: `coop@profile set region <region>`**
> The possible values for `<region>` are: `NA`, `EU`, `ASIA` and `SAR`.

**Setting your profile's world level: `coop@profile set wl <level>`**

**Setting your profile's adventure rank: `coop@profile set ar <level>`**

**Setting your profile's UID: `coop@profile set uid <uid>`**

**Setting your profile's UID public: `coop@profile make uid public`**

**Setting your profile's UID private: `coop@profile make uid private`**

**Viewing your own profile: `coop@profile view`**

**Viewing a Traveler's Profile: `coop@profile view <user>`**
> The possible values for `<user>` are a mention, or the Discord Tag.

**Viewing all opened commissions: `coop@commission list`**

**Posting a new commission: `coop@commission create <type> <message>`**
> The possible values for `<type>` are `boss`, `bounty`, `domain`, `mob`, `resource`,
> `challenge`, `wei` and `other`.
>
> The `<message>` can be whatever you want. For example, if you choose a boss commission,
> you could tell which one you want and how many times.

**Closing your opened commission: `coop@commission close`**

## Things to note

- A commission will stay opened for 10 minutes. After that, it will be automatically closed.
- Any abuse of the commission system will result in a restriction (temporary or definitive).
- You cannot change the bot prefix (`coop@`)
- If you want a new feature, have a bug or a question, do not hesitate to use the issue system or contact me on
  Discord (`Akio Nakao#0001`)

## Contribute

If you want to add something yourself, you can contribute to this project if you want, but you may want to open an issue
to talk about it beforehand.
